﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

using System.Web.Security;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using CSMarketplace.Models;
using System.Data.Entity.Validation;

using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Owin.Host.SystemWeb;


namespace CSMarketplace.Controllers
{
    public class AuthorizeController : ApiController
    {


        public AuthorizeController()
            : this(new UserManager<User>(new UserStore<User>(new ApplicationDbContext())))
        {
        }

        public AuthorizeController(UserManager<User> userManager)
        {
            UserManager = userManager;
            var userValidator = UserManager.UserValidator as UserValidator<User>;
            //userValidator.AllowOnlyAlphanumericUserNames = false;

        }

        public UserManager<User> UserManager { get; private set; }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;

            }
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/caalhp/authorize
        [Route("api/caalhp/authorize/{PlatformId}")]
        [HttpPost]
        public async Task<Boolean>  AuthorizeCAALHP(String PlatformId)
        {

            Guid myGuidStr;

            try
            {


                myGuidStr = Guid.Parse(PlatformId);

            }
            catch 
            {

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return false;

            }

            var platforms = db.Platforms.Where(p => p.PlatformGUID == myGuidStr).ToList();

            if(platforms == null){


                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return false;
            }

            if (platforms.Count() > 0)
            {
                var user = await UserManager.FindAsync("caalhp@carestore.eu", "123456");
                if (user != null)
                {
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);


                     return true;

                }
                else
                {

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    return false;


                }

            }
            else
            {

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                     return false;
            }



        }


        // GET: api/manager/dummy
        [Route("api/manager/dummy")]
        [HttpGet]
        public MarketplaceContract.UserCredentialsData GetDummy()
        {
            MarketplaceContract.UserCredentialsData UserData = new MarketplaceContract.UserCredentialsData();

            UserData.UserLogin = "manager@carestore.eu";
            UserData.UserPassword = "123456";
            return UserData;
        }




        private async Task <Boolean> SignInAsync(MarketplaceContract.UserCredentialsData UserData, bool isPersistent)
        {

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var user = await UserManager.FindAsync(UserData.UserLogin, UserData.UserPassword);

            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            var authenticationProperties = new AuthenticationProperties()
            {
                IsPersistent = isPersistent,
                 
            };

            AuthenticationManager.SignIn(authenticationProperties, identity);
           

            return true;
        }


        // POST: api/manager/authorize
        [Route("api/manager/authorize")]
        [HttpPost]
        public async Task<Boolean> AuthorizeManager(MarketplaceContract.UserCredentialsData UserData)
        {


            

            var user = await UserManager.FindAsync(UserData.UserLogin, UserData.UserPassword);

            Boolean result = await SignInAsync(UserData, false); 

          //  Boolean result2 = AuthenticationManager.User.IsInRole("Care Administrator");

            /*for (var i = 0; i < 3000; i++)
            {

                

                result2 = AuthenticationManager.User.IsInRole("Care Administrator");

                if (result2 == true)
                {
                    break;
                }
            }*/


              //  if (result2 && result)
              //  {
                  //  return AuthenticationManager.User.Identity.GetUserId();

             //   }
                if (result)
                {
                    return true;
                }
                else
                {

                    return false;
                }

            /*
            var user = await UserManager.FindAsync(UserData.UserLogin, UserData.UserPassword);
            if (user != null)
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);

                    


                //AuthenticationManager.AuthenticateAsync(new AuthenticationProperties() { IsPersistent = false }, identity);

                bool result = AuthenticationManager.User.IsInRole("Care Administrator");

              //  return AuthenticationManager.User.Identity.GetUserId();


                if (result)
                {
                    return AuthenticationManager.User.Identity.GetUserId();

                }else{

                    return "0";
                }

            }
            else
            {

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return "-1"; 
            }*/
        }

        // GEET: api/caalhp/isauthenticated
        [Route("api/caalhp/isauthenticated")]
        [HttpGet]
        public String GetCaalhp()
        {
            String myStr = AuthenticationManager.User.IsInRole("CAALHP").ToString();
            return myStr;
        }

        // GET: api/identity/get
        [Route("api/identity/get")]
        [HttpGet]
        public  String GetIdentity()
        {
           String myStr = AuthenticationManager.User.Identity.Name.ToString();
            return myStr;
        }

        // GET: api/manager/isauthenticated
        [Route("api/manager/isauthenticated")]
        [HttpGet]
        public String GetManager()
        {
            String myStr = AuthenticationManager.User.IsInRole("Care Administrator").ToString();
            return myStr;
        }

        // GET: api/manager/isauthenticated
        [Route("api/manager/getid")]
        [HttpGet]
        public String GetManagerId()
        {
            String myStr = "0";

            if (AuthenticationManager.User.IsInRole("Care Administrator"))
            {
                myStr = AuthenticationManager.User.Identity.GetUserId();
            }
            
            return myStr;
        }


    }
}
