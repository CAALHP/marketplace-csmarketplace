﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;


namespace CSMarketplace.Controllers
{
    public class VendorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Vendors/
        public ActionResult Index()
        {
            //return View(db.Vendors.ToList());
            return View(db.Vendors.ToList());
        }

        // GET: /Vendors/List
        public ActionResult List()
        {
            var vendors = from v in db.Vendors select v;
            
            vendors = vendors.Where(v => v.VendorValidation == VendorValidationType.Validated && v.VendorActive==true);

            return View(vendors.ToList());

          //  return View(db.Vendors.ToList());
        }
        

        // GET: /Vendors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendors.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }

        // GET: /Vendors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Vendors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="VendorId,VendorName,VendorAddress,VendorPhone,VendorFax,VendorWebLink,VendorDescription,VendorActive,VendorValidation,VendorLogo,VendorSelf,ApplicationUserID")] Vendor vendor)
        {
            if (ModelState.IsValid)
            {
                db.Vendors.Add(vendor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vendor);
        }


        // GET: /Vendors/VendorUpdate/user_id
        public ActionResult VendorUpdate(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendors.First(u => u.ApplicationUserId == id);
                
              //  .Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }


        // POST: /Vendors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VendorUpdate([Bind(Include = "VendorId,VendorName,VendorAddress,VendorPhone,VendorFax,VendorWebLink,VendorDescription,ApplicationUserId, VendorLogo, VendorActive, VendorValidation, VendorSelf")] Vendor vendor)
       // public ActionResult VendorUpdate([Bind(Exclude = "ApplicationUserId")] Vendor vendor)      
        {

          //  Vendor vendor_original = db.Vendors.First(u => u.VendorId == vendor.VendorId);

          //  vendor.ApplicationUserId = vendor_original.ApplicationUserId;
           // vendor.VendorLogo = vendor_original.VendorLogo;


            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
                //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0


            var current_file = Request.Files[0];


            if (current_file.ContentLength>0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceUserData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {
                    //string fileName = Path.GetFileName(Request.Files[fileNum].FileName);

                    string fileName = "logo_" + Server.UrlEncode(vendor.VendorId.ToString()+vendor.VendorName)+DateTime.Now.ToString("yyyyMMddHHmmss")+".png";
                    if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                    {
                        CloudBlockBlob azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                        azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);


                        vendor.VendorLogo = azureBlockBlob.Uri.ToString();


                    }
                }
            }








            if (ModelState.IsValid)
            {
                db.Entry(vendor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("VendorUpdate","Vendors", new { id=vendor.ApplicationUserId });
            }
            return View(vendor);
        }


        // GET: /Vendors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendors.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }




        // POST: /Vendors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="VendorId,VendorName,VendorAddress,VendorPhone,VendorFax,VendorWebLink,VendorDescription,VendorActive,VendorValidation,VendorLogo,VendorSelf,ApplicationUserID")] Vendor vendor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vendor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vendor);
        }

        // GET: /Vendors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendors.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }

        // POST: /Vendors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vendor vendor = db.Vendors.Find(id);

            string userId = vendor.ApplicationUserId;

            db.Vendors.Remove(vendor);
            db.SaveChanges();


            var user = db.Users.First(u => u.Id == userId);
            db.Users.Remove(user);
            db.SaveChanges();


            return RedirectToAction("Index");




        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
