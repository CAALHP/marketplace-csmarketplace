﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using CSMarketplace.Other;

namespace CSMarketplace.Controllers
{
    public class PlatformsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Platforms
        public async Task <ActionResult> Index()
        {


            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            /*var platforms_list = await (from p in db.Platforms
                        join a in db.Areas
                        on p.CareAreaId equals a.AreaId
                                  where a.OrganizationId == organization_id
                                  select p).ToListAsync();*/



            var platforms_list = await db.Platforms.Include(c => c.CareArea).Where(ca=>ca.CareArea.OrganizationId==organization_id).ToListAsync();


            return View(platforms_list);

           // return (await db.Platforms.Include(c => c.CareArea).ToListAsync());
            //return View(db.Platforms.ToList());
        }

        public ActionResult AdminIndex(string sortOrder, string currentFilter, string search, int? page)
        {

            ViewBag.CurrentSort = sortOrder;

            if (page == null || page == 0)
            {

                page = 1;

            }


            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;


            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var platforms = from a in db.Platforms
                               select a;
            if (!String.IsNullOrEmpty(search))
            {
                platforms = platforms.Where(a => a.PlatformName.ToUpper().Contains(search.ToUpper())
                                       || a.CareOrganization.OrganizationName.ToUpper().Contains(search.ToUpper()));
            }
            else
            {

                platforms = platforms.Where(a => a.PlatformId > 0);

            }

            switch (sortOrder)
            {
                case "name_desc":
                    platforms = platforms.OrderByDescending(a => a.PlatformName);
                    ViewBag.NameSortParm = "name_asc";
                    ViewBag.OrganizationSortParm = "organization_desc";
                    break;
                case "name_asc":
                    platforms = platforms.OrderBy(a => a.PlatformName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.OrganizationSortParm = "organization_desc";
                    break;
                case "organization_desc":
                    platforms = platforms.OrderByDescending(a => a.CareOrganization.OrganizationName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.OrganizationSortParm = "organization_asc";
                    break;
                case "organization_asc":
                    platforms = platforms.OrderBy(a => a.CareOrganization.OrganizationName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.OrganizationSortParm = "organization_desc";
                    break;

                default:
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.OrganizationSortParm = "organization_desc";

                    break;
            }

            ViewBag.SortOrder = sortOrder;

            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = platforms.Count();

            Decimal calculus = (Decimal)platforms.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;

            return View(platforms.ToList());








           // var platforms_list = await db.Platforms.Include(c => c.CareOrganization).ToListAsync();


           // return View(platforms_list);

       
        }

        // GET: Platforms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Platform platform = db.Platforms.Find(id);
            if (platform == null)
            {
                return HttpNotFound();
            }
            return View(platform);
        }

        // GET: Platforms/Create
        public ActionResult Create()
        {

            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;


            return View();
        }

        // POST: Platforms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PlatformId,PlatformName,PlatformMAC,PlatformOS,CareAreaId")] Platform platform)
        {

            var user_id = User.Identity.GetUserId();

            Organization organization = db.Organizations.Where(o => o.AdministratorUserId == user_id).First();
            int organization_id = organization.OrganizationId;

            ViewBag.organization_id = organization_id;

            Area area = db.Areas.Where(a => a.AreaId == platform.CareAreaId).First();
            platform.CareArea = area;

            Guid g = Guid.NewGuid();

            platform.PlatformGUID = g;
            platform.CareOrganizationId = organization_id;
            platform.CareOrganization=organization;

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;


            if (ModelState.IsValid)
            {
                db.Platforms.Add(platform);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(platform);
        }

        // GET: Platforms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Platform platform = db.Platforms.Find(id);
            if (platform == null)
            {
                return HttpNotFound();
            }



            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;


            return View(platform);
        }

        // POST: Platforms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PlatformId,PlatformName,PlatformMAC,PlatformOS,PlatformGUID,CareAreaId")] Platform platform)
        {

            var user_id = User.Identity.GetUserId();

            Organization organization = db.Organizations.Where(o => o.AdministratorUserId == user_id).First();
            int organization_id = organization.OrganizationId;

            ViewBag.organization_id = organization_id;

            Area area = db.Areas.Where(a => a.AreaId == platform.CareAreaId).First();
            platform.CareArea = area;

            platform.CareOrganizationId = organization_id;
            platform.CareOrganization = organization;


            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;




            if (ModelState.IsValid)
            {
                db.Entry(platform).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(platform);
        }

        public CustomFileResult GetConfigFile(String str)
        {
            return CustomFile(str, "text/plain", "caalhp.id");
        }

        protected internal CustomFileResult CustomFile(string content, string contentType, string downloadFileName)
        {
            return new CustomFileResult(content, contentType, downloadFileName);
        }





        // GET: Platforms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Platform platform = db.Platforms.Find(id);
            if (platform == null)
            {
                return HttpNotFound();
            }
            return View(platform);
        }

        // POST: Platforms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Platform platform = db.Platforms.Find(id);
            db.Platforms.Remove(platform);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
