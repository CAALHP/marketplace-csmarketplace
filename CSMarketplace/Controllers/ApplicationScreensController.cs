﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Security.Claims;
using CSMarketplace.Models;

namespace CSMarketplace.Controllers
{
    public class ApplicationScreensController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ApplicationScreens
        public ActionResult Index(int? application_id)
        {
            var applicationScreens = db.ApplicationScreens.Where(c => c.ApplicationID==application_id).OrderBy(s => s.ApplicationScreenSortIndex);
            ViewBag.ApplicationID = application_id;
            //Include(a => a.Application)
            //
            return View(applicationScreens.ToList());
        }

        // GET: ApplicationScreens
        public ActionResult VendorIndex(int? application_id)
        {
            var applicationScreens = db.ApplicationScreens.Where(c => c.ApplicationID == application_id).OrderBy(s => s.ApplicationScreenSortIndex);
            ViewBag.ApplicationID = application_id;
            //Include(a => a.Application)
            //
            return View(applicationScreens.ToList());
        }

        // GET: ApplicationScreens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(id);
            if (applicationScreen == null)
            {
                return HttpNotFound();
            }
            return View(applicationScreen);
        }

        // GET: ApplicationScreens/Create
        public ActionResult Create(int? application_id)
        {
           // ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationId", "ApplicationName");
            ViewBag.ApplicationID = application_id;
            return View();
        }

        // POST: ApplicationScreens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int application_id,[Bind(Include = "ApplicationScreenId,ApplicationScreenSortIndex,ApplicationID")] ApplicationScreen applicationScreen)
        {

            Boolean status_ok = false;



            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0
            Application application = db.Applications.Find(application_id);
            applicationScreen.ApplicationScreenUrl = "problem";
            applicationScreen.ApplicationID = application_id;
            applicationScreen.Application = application;

            if (applicationScreen.ApplicationScreenSortIndex<1) {

                applicationScreen.ApplicationScreenSortIndex = 1;

            }
           


            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceApplicationsData);

                var storageContainerImages = storageClient.GetContainerReference(
               Properties.Settings.Default.CloudStorageContainerReferenceImagesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "appscreen_" + Server.UrlEncode(applicationScreen.ApplicationID.ToString()) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainerImages.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            applicationScreen.ApplicationScreenUrl = azureBlockBlob.Uri.ToString();

                            status_ok = true;


                            
                        }
                    }
                    



                }
            }



            if (status_ok)
            {
                db.ApplicationScreens.Add(applicationScreen);
                db.SaveChanges();

                return RedirectToAction("Index", "ApplicationScreens", new { application_id = application_id });
            }
            
            ViewBag.ApplicationID = applicationScreen.ApplicationID;
           
           // ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationId", "ApplicationName", applicationScreen.ApplicationID);
            return View(applicationScreen);
        }

        // GET: ApplicationScreens/Create
        public ActionResult VendorCreate(int? application_id)
        {
            // ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationId", "ApplicationName");
            ViewBag.ApplicationID = application_id;
            return View();
        }

        // POST: ApplicationScreens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VendorCreate(int application_id, [Bind(Include = "ApplicationScreenId,ApplicationScreenSortIndex,ApplicationID")] ApplicationScreen applicationScreen)
        {

            Boolean status_ok = false;



            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0
            Application application = db.Applications.Find(application_id);
            applicationScreen.ApplicationScreenUrl = "problem";
            applicationScreen.ApplicationID = application_id;
            applicationScreen.Application = application;

            if (applicationScreen.ApplicationScreenSortIndex < 1)
            {

                applicationScreen.ApplicationScreenSortIndex = 1;

            }



            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceApplicationsData);

                var storageContainerImages = storageClient.GetContainerReference(
               Properties.Settings.Default.CloudStorageContainerReferenceImagesData);

                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "appscreen_" + Server.UrlEncode(applicationScreen.ApplicationID.ToString()) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainerImages.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            applicationScreen.ApplicationScreenUrl = azureBlockBlob.Uri.ToString();

                            status_ok = true;



                        }
                    }




                }
            }



            if (status_ok)
            {
                db.ApplicationScreens.Add(applicationScreen);
                db.SaveChanges();

                return RedirectToAction("VendorIndex", "ApplicationScreens", new { application_id = application_id });
            }

            ViewBag.ApplicationID = applicationScreen.ApplicationID;

            // ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationId", "ApplicationName", applicationScreen.ApplicationID);
            return View(applicationScreen);
        }

        // GET: ApplicationScreens/Edit/5
        public ActionResult Edit(int as_id, int application_id)
        {
            if (as_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(as_id);
            if (applicationScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationID = application_id;
            return View(applicationScreen);
        }

        // POST: ApplicationScreens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int application_id, [Bind(Include = "ApplicationScreenId,ApplicationScreenUrl,ApplicationScreenSortIndex,ApplicationID")] ApplicationScreen applicationScreen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationScreen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "ApplicationScreens", new { application_id = application_id });
            }
            ViewBag.ApplicationID = application_id;
            return View(applicationScreen);
        }

        // GET: ApplicationScreens/Edit/5
        public ActionResult VendorEdit(int as_id, int application_id)
        {
            if (as_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(as_id);
            if (applicationScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationID = application_id;
            return View(applicationScreen);
        }

        // POST: ApplicationScreens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VendorEdit(int application_id, [Bind(Include = "ApplicationScreenId,ApplicationScreenUrl,ApplicationScreenSortIndex,ApplicationID")] ApplicationScreen applicationScreen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationScreen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("VendorIndex", "ApplicationScreens", new { application_id = application_id });
            }
            ViewBag.ApplicationID = application_id;
            return View(applicationScreen);
        }

        // GET: ApplicationScreens/Delete/5
        public ActionResult Delete(int as_id, int application_id)
        {
            if (as_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(as_id);
            if (applicationScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationID = application_id;
            return View(applicationScreen);
        }

        // POST: ApplicationScreens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int as_id, int application_id)
        {
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(as_id);
            db.ApplicationScreens.Remove(applicationScreen);
            db.SaveChanges();
            return RedirectToAction("Index", "ApplicationScreens", new { application_id = application_id });
        }

        // GET: ApplicationScreens/Delete/5
        public ActionResult VendorDelete(int as_id, int application_id)
        {
            if (as_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(as_id);
            if (applicationScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationID = application_id;
            return View(applicationScreen);
        }

        // POST: ApplicationScreens/Delete/5
        [HttpPost, ActionName("VendorDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult VendorDeleteConfirmed(int as_id, int application_id)
        {
            ApplicationScreen applicationScreen = db.ApplicationScreens.Find(as_id);
            db.ApplicationScreens.Remove(applicationScreen);
            db.SaveChanges();
            return RedirectToAction("VendorIndex", "ApplicationScreens", new { application_id = application_id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
