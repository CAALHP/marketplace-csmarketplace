﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CSMarketplace.Models;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace CSMarketplace.Controllers
{
    public class DriversWebApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/drivers
        [Authorize(Roles = "CAALHP")] 
        [Route("api/drivers")]
        public IEnumerable<MarketplaceContract.Driver> GetDrivers()
        {


            IList<MarketplaceContract.Driver> driversList = new List<MarketplaceContract.Driver>();

            var drivers = db.Drivers.Where(d => d.DriverValidation == DriverValidationType.Validated).ToList();

            CloudStorageAccount account = CloudStorageAccount.Parse(Properties.Settings.Default.StorageConnectionString);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference("drivers");






            foreach (var driverData in drivers)
            {


                var signedBlobUrl = "";

                if (!String.IsNullOrEmpty(driverData.DriverUrl))
                {


                    var driver_filename = Path.GetFileName(Uri.UnescapeDataString(driverData.DriverUrl).Replace("/", "\\"));
                    var blob = container.GetBlockBlobReference(driver_filename);

                    var builder = new UriBuilder(blob.Uri);
                    builder.Query = blob.GetSharedAccessSignature(
                        new SharedAccessBlobPolicy
                        {
                            Permissions = SharedAccessBlobPermissions.Read,
                            SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                            SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(60))
                        }).TrimStart('?');

                    signedBlobUrl = builder.Uri.ToString();
                }
                driversList.Add(new MarketplaceContract.Driver()
                {
                    DriverId = driverData.DriverId,
                    Name = driverData.DriverName,
                    Url = signedBlobUrl,
                });
            }

            return driversList;
        }

        // GET: api/drivers/5
        [Authorize(Roles = "CAALHP")] 
        [Route("api/drivers/{id}")]
        [ResponseType(typeof(MarketplaceContract.DriverProfile))]
        public IHttpActionResult GetDriver(int id)
        {

            Driver driver_db = db.Drivers.Find(id);

            if (driver_db == null)
            {
                return NotFound();
            }

            MarketplaceContract.DriverProfile driver = new MarketplaceContract.DriverProfile();


            CloudStorageAccount account = CloudStorageAccount.Parse(Properties.Settings.Default.StorageConnectionString);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference("drivers");

            var signedBlobUrl = "";

            if (!String.IsNullOrEmpty(driver_db.DriverUrl))
            {

                var driver_filename = Path.GetFileName(Uri.UnescapeDataString(driver_db.DriverUrl).Replace("/", "\\"));
                var blob = container.GetBlockBlobReference(driver_filename);

                var builder = new UriBuilder(blob.Uri);
                builder.Query = blob.GetSharedAccessSignature(
                    new SharedAccessBlobPolicy
                    {
                        Permissions = SharedAccessBlobPermissions.Read,
                        SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                        SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(60))
                    }).TrimStart('?');

                signedBlobUrl = builder.Uri.ToString();
            }

            driver.DriverId = driver_db.DriverId;
            driver.Name = driver_db.DriverName;
            driver.Description = driver_db.DriverDescription;
            driver.Url = signedBlobUrl;

            return Ok(driver);

        }

        // GET: api/drivers/nfc/5
        [Authorize(Roles = "CAALHP")] 
        [Route("api/drivers/nfc/{device_nfc}")]
        [ResponseType(typeof(MarketplaceContract.Driver))]
        public IHttpActionResult GetDriverNfc(string device_nfc)
        {

            Device device_db = db.DeviceEntities.Where(de => de.DeviceEntityNFC == device_nfc).First().Device;



            Driver driver_db = device_db.Drivers.First();

            if (driver_db == null)
            {
                return NotFound();
            }


            CloudStorageAccount account = CloudStorageAccount.Parse(Properties.Settings.Default.StorageConnectionString);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference("drivers");

            var signedBlobUrl = "";

            if (!String.IsNullOrEmpty(driver_db.DriverUrl))
            {

                var driver_filename = Path.GetFileName(Uri.UnescapeDataString(driver_db.DriverUrl).Replace("/", "\\"));
                var blob = container.GetBlockBlobReference(driver_filename);

                var builder = new UriBuilder(blob.Uri);
                builder.Query = blob.GetSharedAccessSignature(
                    new SharedAccessBlobPolicy
                    {
                        Permissions = SharedAccessBlobPermissions.Read,
                        SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                        SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(60))
                    }).TrimStart('?');

               signedBlobUrl = builder.Uri.ToString();
            }
            MarketplaceContract.Driver driver = new MarketplaceContract.Driver();

            driver.DriverId = driver_db.DriverId;
            driver.Name = driver_db.DriverName;
            driver.Url = signedBlobUrl;

            return Ok(driver);

        }

        // GET: api/drivers/nfc/5/all
        [Authorize(Roles = "CAALHP")] 
        [Route("api/drivers/nfc/{device_nfc}/all")]
        public IEnumerable<MarketplaceContract.Driver> GetDriverNfcAll(string device_nfc)
        {

            Device device_db = db.DeviceEntities.Where(de => de.DeviceEntityNFC == device_nfc).First().Device;

            CloudStorageAccount account = CloudStorageAccount.Parse(Properties.Settings.Default.StorageConnectionString);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference("drivers");

            IList<MarketplaceContract.Driver> driversList = new List<MarketplaceContract.Driver>();
            var drivers_db = device_db.Drivers.ToList();

            foreach (var driverData in drivers_db)
            {

                var signedBlobUrl = "";

                if (!String.IsNullOrEmpty(driverData.DriverUrl))
                {

                    var driver_filename = Path.GetFileName(Uri.UnescapeDataString(driverData.DriverUrl).Replace("/", "\\"));
                    var blob = container.GetBlockBlobReference(driver_filename);

                    var builder = new UriBuilder(blob.Uri);
                    builder.Query = blob.GetSharedAccessSignature(
                        new SharedAccessBlobPolicy
                        {
                            Permissions = SharedAccessBlobPermissions.Read,
                            SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                            SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(60))
                        }).TrimStart('?');

                    signedBlobUrl = builder.Uri.ToString();

                }
                driversList.Add(new MarketplaceContract.Driver()
                {
                    DriverId = driverData.DriverId,
                    Name = driverData.DriverName,
                    Url = signedBlobUrl,
                });
            }

            return driversList;

        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DriverExists(int id)
        {
            return db.Drivers.Count(e => e.DriverId == id) > 0;
        }
    }
}