﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using CSMarketplace.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CSMarketplace.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {




        public AccountController()
            : this(new UserManager<User>(new UserStore<User>(new ApplicationDbContext())))
        {



           // UserValidator = new UserValidator<User>(this) { AllowOnlyAlphanumericUserNames = false };

        }

        public AccountController(UserManager<User> userManager)
        {
            
            UserManager = userManager;

            // Start of new code
            UserManager.UserValidator = new UserValidator<User>(UserManager)
            {
                AllowOnlyAlphanumericUserNames = false,
            };

          //  UserValidator<User> uValidator = new UserValidator<User>(userManager);

         //   uValidator.AllowOnlyAlphanumericUserNames = false;


         //   UserManager = userManager;

          //  UserManager.UserValidator = uValidator;

          
           // var userValidator = UserManager.UserValidator as UserValidator<User>;
            //userValidator.AllowOnlyAlphanumericUserNames = false;

        }

        public UserManager<User> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Db = new ApplicationDbContext();

                var user = new User() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user, model.Password);
                string role_vendor = "Vendor";

               // var store = new UserStore<ApplicationUser>(context);
               // var manager = new UserManager<ApplicationUser>(store);

                

              

                if (result.Succeeded)
                {
                    var result_role = await UserManager.AddToRoleAsync(user.Id, role_vendor);

                    var current_user = await UserManager.FindByIdAsync(user.Id);
                    if (ModelState.IsValid)
                    {
                        Vendor new_vendor_account = new Vendor();
                        new_vendor_account.ApplicationUserId = current_user.Id;
                        new_vendor_account.VendorActive = true;

                        //new_vendor_account.ApplicationUser = current_user;



                        Db.Vendors.Add(new_vendor_account);
                        try
                        {
                            await Db.SaveChangesAsync();
                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {

                                    var pp= validationError.PropertyName;
                                    var rr=validationError.ErrorMessage;
                                   // var zz=0;
                                }
                            }
                        }
                        
                        // Db.SaveChanges();
                       
                    }
                    var errors = ModelState.Values.SelectMany(v => v.Errors);

                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }




        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var Db = new ApplicationDbContext();
            var users = Db.Users;
            var model = new List<EditUserViewModel>();
            foreach (var user in users)
            {
                var u = new EditUserViewModel(user);
                model.Add(u);
            }
            return View(model);
        }

        public ActionResult VendorsUsers(string currentFilter, string search, int? page)
        {

            if (page == null || page == 0)
            {
                page = 1;
            }

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var Db = new ApplicationDbContext();

            var role = Db.Roles
                    .Where(b => b.Name == "Vendor")
                    .FirstOrDefault();

            IList<User> vendorsList = new List<User>();

            var vendors = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            foreach (var vendor in vendors)
            {


                if (!String.IsNullOrEmpty(search))
                {

                    string compare_str = "";

                    if (!String.IsNullOrEmpty(vendor.FirstName))
                    {
                        compare_str += vendor.FirstName + " ";

                    }

                    if (!String.IsNullOrEmpty(vendor.LastName))
                    {
                        compare_str += vendor.LastName + " ";

                    }

                    if (!String.IsNullOrEmpty(vendor.UserName))
                    {
                        compare_str += vendor.UserName + " ";

                    }

                    if (!String.IsNullOrEmpty(vendor.Vendors.First().VendorName))
                    {
                        compare_str += vendor.Vendors.First().VendorName + " ";

                    }


                    if (compare_str.ToUpper().Contains(search.ToUpper()) == true)
                    {
                        vendorsList.Add(vendor);
                    }



                }
                else
                {

                    vendorsList.Add(vendor);

                }

            }

          

            //  var users = Db.Users;
            var model = new List<EditUserViewModel>();
            foreach (var user in vendorsList)
            {
                var u = new EditUserViewModel(user);



                model.Add(u);
            }


            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = vendorsList.Count();

            Decimal calculus = (Decimal)vendorsList.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;




            // return View(citizensList);
            //return View(model);
            return View(vendorsList);
        }

        public ActionResult ManagersUsers(string currentFilter, string search, int? page)
        {

            if (page == null || page == 0)
            {
                page = 1;
            }

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var Db = new ApplicationDbContext();

            var role = Db.Roles
                    .Where(b => b.Name == "Care Administrator")
                    .FirstOrDefault();

            IList<User> managersList = new List<User>();

            var managers = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            foreach (var manager in managers)
            {


                if (!String.IsNullOrEmpty(search))
                {

                    string compare_str = "";

                    if (!String.IsNullOrEmpty(manager.FirstName))
                    {
                        compare_str += manager.FirstName + " ";

                    }

                    if (!String.IsNullOrEmpty(manager.LastName))
                    {
                        compare_str += manager.LastName + " ";

                    }

                    if (!String.IsNullOrEmpty(manager.UserName))
                    {
                        compare_str += manager.UserName + " ";

                    }

                    if (!String.IsNullOrEmpty(manager.Organizations.First().OrganizationName))
                    {
                        compare_str += manager.Organizations.First().OrganizationName + " ";

                    }


                    if (compare_str.ToUpper().Contains(search.ToUpper()) == true)
                    {
                        managersList.Add(manager);
                    }



                }
                else
                {

                    managersList.Add(manager);

                }

            }



            //  var users = Db.Users;
            var model = new List<EditUserViewModel>();
            foreach (var user in managersList)
            {
                var u = new EditUserViewModel(user);



                model.Add(u);
            }


            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = managersList.Count();

            Decimal calculus = (Decimal)managersList.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;




            // return View(citizensList);
            //return View(model);
            return View(managersList);
        }

        public ActionResult CitizensUsers(string currentFilter, string search, int? page)
        {

            if (page == null || page == 0)
            {
                page = 1;
            }

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var Db = new ApplicationDbContext();

            var role = Db.Roles
                    .Where(b => b.Name == "Citizen")
                    .FirstOrDefault();

            IList<User> citizensList = new List<User>();

            var citizens = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            foreach (var citizen in citizens)
            {

               
                    if (!String.IsNullOrEmpty(search))
                    {

                        string compare_str = "";

                        if (!String.IsNullOrEmpty(citizen.FirstName))
                        {
                            compare_str += citizen.FirstName + " ";

                        }

                        if (!String.IsNullOrEmpty(citizen.LastName))
                        {
                            compare_str += citizen.LastName + " ";

                        }

                        String myOrganization = Db.Organizations.Find(citizen.OrganizationRelationID).OrganizationName;

                        if (!String.IsNullOrEmpty(myOrganization))
                        {
                            compare_str += myOrganization + " ";

                        }


                        if (compare_str.ToUpper().Contains(search.ToUpper()) == true)
                        {
                            citizensList.Add(citizen);
                        }



                    }
                    else
                    {

                        citizensList.Add(citizen);

                    }

            }





            //  var users = Db.Users;
            var model = new List<EditUserViewModel>();

            ICollection<KeyValuePair<int, String>> organizationsList =
            new Dictionary<int, String>();

            var organizations = Db.Organizations.ToList();

            foreach (var organization in organizations)
            {
                organizationsList.Add(new KeyValuePair<int, String>(organization.OrganizationId, organization.OrganizationName));
            }





            foreach (var user in citizensList)
            {
                var u = new EditUserViewModel(user);

                

                model.Add(u);
            }


            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = citizensList.Count();

            Decimal calculus = (Decimal)citizensList.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;


            ViewBag.OrganizationsList = organizationsList;



            // return View(citizensList);
            return View(citizensList);
        }


        public ActionResult CaregiversUsers(string currentFilter, string search, int? page)
        {

            if (page == null || page == 0)
            {
                page = 1;
            }

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var Db = new ApplicationDbContext();

            var role = Db.Roles
                    .Where(b => b.Name == "Care Giver")
                    .FirstOrDefault();

            IList<User> caregiversList = new List<User>();

            var caregivers = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            foreach (var caregiver in caregivers)
            {


                if (!String.IsNullOrEmpty(search))
                {

                    string compare_str = "";

                    if (!String.IsNullOrEmpty(caregiver.FirstName))
                    {
                        compare_str += caregiver.FirstName + " ";

                    }

                    if (!String.IsNullOrEmpty(caregiver.LastName))
                    {
                        compare_str += caregiver.LastName + " ";

                    }

                    String myOrganization = Db.Organizations.Find(caregiver.OrganizationRelationID).OrganizationName;

                    if (!String.IsNullOrEmpty(myOrganization))
                    {
                        compare_str += myOrganization + " ";

                    }


                    if (compare_str.ToUpper().Contains(search.ToUpper()) == true)
                    {
                        caregiversList.Add(caregiver);
                    }



                }
                else
                {

                    caregiversList.Add(caregiver);

                }

            }





            //  var users = Db.Users;
            var model = new List<EditUserViewModel>();

            ICollection<KeyValuePair<int, String>> organizationsList =
            new Dictionary<int, String>();

            var organizations = Db.Organizations.ToList();

            foreach (var organization in organizations)
            {
                organizationsList.Add(new KeyValuePair<int, String>(organization.OrganizationId, organization.OrganizationName));
            }





            foreach (var user in caregiversList)
            {
                var u = new EditUserViewModel(user);



                model.Add(u);
            }


            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = caregiversList.Count();

            Decimal calculus = (Decimal)caregiversList.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;


            ViewBag.OrganizationsList = organizationsList;



            // return View(citizensList);
            return View(caregiversList);
        }



        public ActionResult CitizensList(string currentFilter, string search, int? page)
        {

            if (page == null || page == 0)
            {
                page = 1;
            }

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var Db = new ApplicationDbContext();

            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            var role = Db.Roles
                    .Where(b => b.Name == "Citizen")
                    .FirstOrDefault();

            IList<User> citizensList = new List<User>();
        
            var citizens = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            foreach(var citizen in citizens){

                if (citizen.OrganizationRelationID == organization_id)
                {
                    if (!String.IsNullOrEmpty(search))
                    {



                        string compare_str = "";

                        if (!String.IsNullOrEmpty(citizen.FirstName))
                        {
                            compare_str += citizen.FirstName + " ";

                        }

                        if (!String.IsNullOrEmpty(citizen.LastName))
                        {
                            compare_str += citizen.LastName + " ";

                        }


                        if (compare_str.ToUpper().Contains(search.ToUpper()) == true)
                            {
                                citizensList.Add(citizen);
                            }
                        
                        

                    }
                    else
                    {

                        citizensList.Add(citizen);

                    }
                    
                }

            }

           


            
          //  var users = Db.Users;
            var model = new List<EditUserViewModel>();
            foreach (var user in citizensList)
            {
                var u = new EditUserViewModel(user);

                

                model.Add(u);
            }


            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = citizensList.Count();

            Decimal calculus = (Decimal)citizensList.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;




           // return View(citizensList);
            return View(model);
        }


        public ActionResult CaregiversList(string currentFilter, string search, int? page)
        {

            if (page == null || page == 0)
            {
                page = 1;
            }

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }



            var Db = new ApplicationDbContext();
            var model = new List<EditUserViewModel>();

                var user_id = User.Identity.GetUserId();

                int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

                var role = Db.Roles
                        .Where(b => b.Name == "Care Giver")
                        .FirstOrDefault();

                IList<User> caregiversList = new List<User>();

                var caregivers = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

                foreach (var caregiver in caregivers)
                {

                    if (caregiver.OrganizationRelationID == organization_id)
                    {

                        if (!String.IsNullOrEmpty(search))
                        {

                            string compare_str = "";

                            if (!String.IsNullOrEmpty(caregiver.FirstName))
                            {
                                compare_str += caregiver.FirstName + " ";

                            }

                            if (!String.IsNullOrEmpty(caregiver.LastName))
                            {
                                compare_str += caregiver.LastName + " ";

                            }


                            if (compare_str.ToUpper().Contains(search.ToUpper()) == true)
                            {
                                caregiversList.Add(caregiver);
                            }



                        }
                        else
                        {

                            caregiversList.Add(caregiver);

                        }

                    }

                }


                
                foreach (var user in caregiversList)
                {
                    var u = new EditUserViewModel(user);

                    model.Add(u);
                }


                ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
                ViewBag.PageID = page;
                ViewBag.ItemsNo = caregiversList.Count();

                Decimal calculus = (Decimal)caregiversList.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
                calculus = Math.Ceiling(calculus);
                int pagesqty = Convert.ToInt32(calculus); ;
                ViewBag.PagesQty = pagesqty;

                if (page == 1)
                {

                    ViewBag.PagePrevIndex = 1;
                    ViewBag.PageNextIndex = 2;

                }
                else if (page == pagesqty)
                {
                    ViewBag.PagePrevIndex = pagesqty - 1;
                    ViewBag.PageNextIndex = pagesqty;

                }
                else
                {
                    ViewBag.PagePrevIndex = page - 1;
                    ViewBag.PageNextIndex = page + 1;

                }

                ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;

            

            return View(model);
        }


       // [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id, ManageMessageId? Message = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModel(user);
            ViewBag.MessageId = Message;
            return View(model);
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Db = new ApplicationDbContext();
                var user = Db.Users.First(u => u.Id == model.UserId);

                // Update the user data:
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                //return RedirectToAction("Edit");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        // [Authorize(Roles = "Admin")]
        public ActionResult EditCitizen(string id, ManageMessageId? Message = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModel(user);
            ViewBag.MessageId = Message;

            return View(model);
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCitizen(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Db = new ApplicationDbContext();
                var user = Db.Users.First(u => u.Id == model.UserId);

                // Update the user data:
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                


                var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
                //CloudConfigurationManager.GetSetting("StorageConnectionString");
                //Request.Files.Count > 0

                var files = Request.Files;

                var current_file = Request.Files[0];


                if (current_file.ContentLength > 0)
                {
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                    var storageClient = storageAccount.CreateCloudBlobClient();
                    var storageContainer = storageClient.GetContainerReference(
                    Properties.Settings.Default.CloudStorageContainerReferenceUserData);
                    storageContainer.CreateIfNotExists();
                    for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                    {

                        var file_source = Request.Files.AllKeys[fileNum];

                        CloudBlockBlob azureBlockBlob;
                        string fileName;

                        if (file_source == "FileUploader")
                        {
                            fileName = "userprofile_" + Server.UrlEncode(user.Id.ToString()) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg";
                            if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                            {
                                azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                                azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                                user.ProfilePicture = azureBlockBlob.Uri.ToString();
                            }
                        }
                        



                    }
                }









                Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                return RedirectToAction("CitizensList");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // [Authorize(Roles = "Admin")]
        public  ActionResult EditCitizenPlatforms(string id, ManageMessageId? Message = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new  EditUserViewModelPlatforms(user);
            ViewBag.MessageId = Message;

            

            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

          //  var platforms_list = await Db.Platforms.Include(c => c.CareArea).Where(ca => ca.CareArea.OrganizationId == organization_id);

            
           ViewBag.UserPlatforms = user.Platforms;

         /*   var platforms_list = (from p in Db.Platforms
                                        join a in Db.Areas
                                        on p.CareAreaId equals a.AreaId
                                        where a.OrganizationId == organization_id
                                         select p).ToList();*/

            var platforms_list = (from p in Db.Platforms
                                  where p.CareOrganizationId == organization_id
                                  select p).ToList();


            ViewBag.Platforms = new SelectList(platforms_list, "PlatformId", "PlatformName");
          

            return View(model);
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCitizenPlatforms(EditUserViewModelPlatforms model)
        {

            var Db = new ApplicationDbContext();

            var user = Db.Users.First(u => u.Id == model.UserId);

            if (ModelState.IsValid)
            {

               

                var platform = Db.Platforms.First(p=>p.PlatformId==model.SelectedPlatformID);

               // user.Platforms.

                user.Platforms.Add(platform);



                // Update the user data:
               // user.FirstName = model.FirstName;
              //  user.LastName = model.LastName;
                Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
               // return RedirectToAction("CitizensList");
            }


           
        
           // var model = new EditUserViewModelPlatforms(user);
          //  ViewBag.MessageId = Message;

            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            var platforms_list = (from p in Db.Platforms
                                  where p.CareOrganizationId == organization_id
                                  select p).ToList();

            ViewBag.UserPlatforms = user.Platforms;
            ViewBag.Platforms = new SelectList(platforms_list, "PlatformId", "PlatformName");

            // If we got this far, something failed, redisplay form
            return View(model);
        }




        public ActionResult EditCitizenID(string id, int ? error, string credential_data)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);


            if (error == null || error == 0)
            {

                error = 0;

            }

            ViewBag.Error = error;

            if (credential_data == null)
            {
             
                credential_data = "";
            }
            ViewBag.ErroredCredential = credential_data;
            

            var credentials_list_nfc = (from c in Db.Credentials
                                  where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                  select c).ToList();

            var credentials_list_figerprint = (from c in Db.Credentials
                                  where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                  select c).ToList();


            if (credentials_list_figerprint.Count() > 0)
            {

                ViewBag.FingerprintExist = 1;
            }
            else
            {

                ViewBag.FingerprintExist = 0;
            }

            if (credentials_list_nfc.Count() > 0)
            {

                ViewBag.UserNFC = credentials_list_nfc.First().CredentialData;
            }
            else
            {

                Credential newNFC = new Credential();

                newNFC.CarestoreUser = user;

                newNFC.CredentialData = id;
                newNFC.CredentialMetric = CredentialMetricType.NFC;

                Db.Credentials.Add(newNFC);
                Db.SaveChanges();

                ViewBag.UserNFC = newNFC.CredentialData;

            }

            var credentials_nfc = (from c in Db.Credentials
                                    where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                    select c).ToList().First();


            ViewBag.UserName = user.FirstName + " " + user.LastName;
            ViewBag.UserID = id;

            return View(credentials_nfc);
        }

        public ActionResult EditCitizenIDAdmin(string id, int? error, string credential_data)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);


            if (error == null || error == 0)
            {

                error = 0;

            }

            ViewBag.Error = error;

            if (credential_data == null)
            {

                credential_data = "";
            }
            ViewBag.ErroredCredential = credential_data;


            var credentials_list_nfc = (from c in Db.Credentials
                                        where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                        select c).ToList();

            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            if (credentials_list_figerprint.Count() > 0)
            {

                ViewBag.FingerprintExist = 1;
            }
            else
            {

                ViewBag.FingerprintExist = 0;
            }

            if (credentials_list_nfc.Count() > 0)
            {

                ViewBag.UserNFC = credentials_list_nfc.First().CredentialData;
            }
            else
            {

                Credential newNFC = new Credential();

                newNFC.CarestoreUser = user;

                newNFC.CredentialData = id;
                newNFC.CredentialMetric = CredentialMetricType.NFC;

                Db.Credentials.Add(newNFC);
                Db.SaveChanges();

                ViewBag.UserNFC = newNFC.CredentialData;

            }

            var credentials_nfc = (from c in Db.Credentials
                                   where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                   select c).ToList().First();


            ViewBag.UserName = user.FirstName + " " + user.LastName;
            ViewBag.UserID = id;

            return View(credentials_nfc);
        }


        public ActionResult EditCaregiverID(string id, int? error, string credential_data)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);


            if (error == null || error == 0)
            {

                error = 0;

            }

            ViewBag.Error = error;

            if (credential_data == null)
            {

                credential_data = "";
            }
            ViewBag.ErroredCredential = credential_data;


            var credentials_list_nfc = (from c in Db.Credentials
                                        where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                        select c).ToList();

            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            if (credentials_list_figerprint.Count() > 0)
            {

                ViewBag.FingerprintExist = 1;
            }
            else
            {

                ViewBag.FingerprintExist = 0;
            }

            if (credentials_list_nfc.Count() > 0)
            {

                ViewBag.UserNFC = credentials_list_nfc.First().CredentialData;
            }
            else
            {

                Credential newNFC = new Credential();

                newNFC.CarestoreUser = user;

                newNFC.CredentialData = id;
                newNFC.CredentialMetric = CredentialMetricType.NFC;

                Db.Credentials.Add(newNFC);
                Db.SaveChanges();

                ViewBag.UserNFC = newNFC.CredentialData;

            }

            var credentials_nfc = (from c in Db.Credentials
                                   where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                   select c).ToList().First();


            ViewBag.UserName = user.FirstName + " " + user.LastName;
            ViewBag.UserID = id;

            return View(credentials_nfc);
        }

        public ActionResult EditCaregiverIDAdmin(string id, int? error, string credential_data)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);


            if (error == null || error == 0)
            {

                error = 0;

            }

            ViewBag.Error = error;

            if (credential_data == null)
            {

                credential_data = "";
            }
            ViewBag.ErroredCredential = credential_data;


            var credentials_list_nfc = (from c in Db.Credentials
                                        where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                        select c).ToList();

            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            if (credentials_list_figerprint.Count() > 0)
            {

                ViewBag.FingerprintExist = 1;
            }
            else
            {

                ViewBag.FingerprintExist = 0;
            }

            if (credentials_list_nfc.Count() > 0)
            {

                ViewBag.UserNFC = credentials_list_nfc.First().CredentialData;
            }
            else
            {

                Credential newNFC = new Credential();

                newNFC.CarestoreUser = user;

                newNFC.CredentialData = id;
                newNFC.CredentialMetric = CredentialMetricType.NFC;

                Db.Credentials.Add(newNFC);
                Db.SaveChanges();

                ViewBag.UserNFC = newNFC.CredentialData;

            }

            var credentials_nfc = (from c in Db.Credentials
                                   where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.NFC
                                   select c).ToList().First();


            ViewBag.UserName = user.FirstName + " " + user.LastName;
            ViewBag.UserID = id;

            return View(credentials_nfc);
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCitizenNFC(Credential model)
        {
           
                var Db = new ApplicationDbContext();
                var user = Db.Users.First(u => u.Id == model.CarestoreUserId);


                Boolean isIn = false;


               if(!String.IsNullOrEmpty(model.CredentialData)){



                   var credentials_list_nfc = (from c in Db.Credentials
                                               where c.CredentialData == model.CredentialData && c.CredentialMetric == CredentialMetricType.NFC
                                               select c).ToList();


                   foreach (Credential credentialTemp in credentials_list_nfc)
                   {

                       var userTemp = Db.Users.First(u => u.Id == credentialTemp.CarestoreUserId);

                       if (userTemp.OrganizationRelationID == user.OrganizationRelationID)
                       {
                           isIn = true;
                       }


                   }

                }


               if (isIn)
               {


                   return RedirectToAction("EditCitizenID", new { id = user.Id, error = 1, credential_data = model.CredentialData });

               }
               else { 


                    var credential = Db.Credentials.First(c => c.CredentialId == model.CredentialId);

                    credential.CredentialData = model.CredentialData;

                    Db.Entry(credential).State = System.Data.Entity.EntityState.Modified;
                    await Db.SaveChangesAsync();
                    return RedirectToAction("EditCitizenID", new { id = user.Id });

               }
           
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCitizenNFCAdmin(Credential model)
        {

            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == model.CarestoreUserId);


            Boolean isIn = false;


            if (!String.IsNullOrEmpty(model.CredentialData))
            {



                var credentials_list_nfc = (from c in Db.Credentials
                                            where c.CredentialData == model.CredentialData && c.CredentialMetric == CredentialMetricType.NFC
                                            select c).ToList();


                foreach (Credential credentialTemp in credentials_list_nfc)
                {

                    var userTemp = Db.Users.First(u => u.Id == credentialTemp.CarestoreUserId);

                    if (userTemp.OrganizationRelationID == user.OrganizationRelationID)
                    {
                        isIn = true;
                    }


                }

            }


            if (isIn)
            {


                return RedirectToAction("EditCitizenIDAdmin", new { id = user.Id, error = 1, credential_data = model.CredentialData });

            }
            else
            {


                var credential = Db.Credentials.First(c => c.CredentialId == model.CredentialId);

                credential.CredentialData = model.CredentialData;

                Db.Entry(credential).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                return RedirectToAction("EditCitizenIDAdmin", new { id = user.Id });

            }

        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCaregiverNFC(Credential model)
        {

            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == model.CarestoreUserId);


            Boolean isIn = false;


            if (!String.IsNullOrEmpty(model.CredentialData))
            {



                var credentials_list_nfc = (from c in Db.Credentials
                                            where c.CredentialData == model.CredentialData && c.CredentialMetric == CredentialMetricType.NFC
                                            select c).ToList();


                foreach (Credential credentialTemp in credentials_list_nfc)
                {

                    var userTemp = Db.Users.First(u => u.Id == credentialTemp.CarestoreUserId);

                    if (userTemp.OrganizationRelationID == user.OrganizationRelationID)
                    {
                        isIn = true;
                    }


                }

            }


            if (isIn)
            {


                return RedirectToAction("EditCaregiverID", new { id = user.Id, error = 1, credential_data = model.CredentialData });

            }
            else
            {


                var credential = Db.Credentials.First(c => c.CredentialId == model.CredentialId);

                credential.CredentialData = model.CredentialData;

                Db.Entry(credential).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                return RedirectToAction("EditCaregiverID", new { id = user.Id });

            }

        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCaregiverNFCAdmin(Credential model)
        {

            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == model.CarestoreUserId);


            Boolean isIn = false;


            if (!String.IsNullOrEmpty(model.CredentialData))
            {



                var credentials_list_nfc = (from c in Db.Credentials
                                            where c.CredentialData == model.CredentialData && c.CredentialMetric == CredentialMetricType.NFC
                                            select c).ToList();


                foreach (Credential credentialTemp in credentials_list_nfc)
                {

                    var userTemp = Db.Users.First(u => u.Id == credentialTemp.CarestoreUserId);

                    if (userTemp.OrganizationRelationID == user.OrganizationRelationID)
                    {
                        isIn = true;
                    }


                }

            }


            if (isIn)
            {


                return RedirectToAction("EditCaregiverIDAdmin", new { id = user.Id, error = 1, credential_data = model.CredentialData });

            }
            else
            {


                var credential = Db.Credentials.First(c => c.CredentialId == model.CredentialId);

                credential.CredentialData = model.CredentialData;

                Db.Entry(credential).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                return RedirectToAction("EditCaregiverIDAdmin", new { id = user.Id });

            }

        }


        public ActionResult EditCaregiverAreas(string id, ManageMessageId? Message = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModelAreas(user);
            ViewBag.MessageId = Message;



            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            //  var platforms_list = await Db.Platforms.Include(c => c.CareArea).Where(ca => ca.CareArea.OrganizationId == organization_id);


            ViewBag.UserAreas = user.Areas;

            /*   var platforms_list = (from p in Db.Platforms
                                           join a in Db.Areas
                                           on p.CareAreaId equals a.AreaId
                                           where a.OrganizationId == organization_id
                                            select p).ToList();*/

            var areas_list = (from a in Db.Areas
                                  where a.OrganizationId == organization_id
                                  select a).ToList();


            ViewBag.Areas = new SelectList(areas_list, "AreaId", "AreaName");


            return View(model);
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCaregiverAreas(EditUserViewModelAreas model)
        {

            var Db = new ApplicationDbContext();

            var user = Db.Users.First(u => u.Id == model.UserId);

            if (ModelState.IsValid)
            {



                var area = Db.Areas.First(a => a.AreaId == model.SelectedAreaID);

                // user.Platforms.

                user.Areas.Add(area);



                // Update the user data:
                // user.FirstName = model.FirstName;
                //  user.LastName = model.LastName;
                Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                // return RedirectToAction("CitizensList");
            }




            // var model = new EditUserViewModelPlatforms(user);
            //  ViewBag.MessageId = Message;

            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            var areas_list = (from a in Db.Areas
                                  where a.OrganizationId == organization_id
                                  select a).ToList();

            ViewBag.UserAreas = user.Areas;
            ViewBag.Areas = new SelectList(areas_list, "AreaId", "AreaName");

            // If we got this far, something failed, redisplay form
            return View(model);
        }



        // [Authorize(Roles = "Admin")]
        public ActionResult EditCaregiver(string id, ManageMessageId? Message = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModel(user);
            ViewBag.MessageId = Message;
            return View(model);
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCaregiver(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Db = new ApplicationDbContext();
                var user = Db.Users.First(u => u.Id == model.UserId);

                // Update the user data:
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;


                var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
                //CloudConfigurationManager.GetSetting("StorageConnectionString");
                //Request.Files.Count > 0

                var files = Request.Files;

                var current_file = Request.Files[0];


                if (current_file.ContentLength > 0)
                {
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                    var storageClient = storageAccount.CreateCloudBlobClient();
                    var storageContainer = storageClient.GetContainerReference(
                    Properties.Settings.Default.CloudStorageContainerReferenceUserData);
                    storageContainer.CreateIfNotExists();
                    for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                    {

                        var file_source = Request.Files.AllKeys[fileNum];

                        CloudBlockBlob azureBlockBlob;
                        string fileName;

                        if (file_source == "FileUploader")
                        {
                            fileName = "userprofile_" + Server.UrlEncode(user.Id.ToString()) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg";
                            if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                            {
                                azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                                azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                                user.ProfilePicture = azureBlockBlob.Uri.ToString();
                            }
                        }




                    }
                }







                Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                await Db.SaveChangesAsync();
                return RedirectToAction("CaregiversList");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

      


       // [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModel(user);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

     //   [HttpPost, ActionName("RemoveUserPlatform")]
      //  [ValidateAntiForgeryToken]
        //  [Authorize(Roles = "Admin")]
        public ActionResult RemoveUserPlatform(string id,int pid)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);

            var platform = Db.Platforms.First(p => p.PlatformId == pid);



            user.Platforms.Remove(platform);
           
            Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return RedirectToAction("EditCitizenPlatforms", "Account", new { id = id});
        }

        public ActionResult RemoveUserArea(string id, int aid)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);

            var area = Db.Areas.First(a => a.AreaId == aid);



            user.Areas.Remove(area);

            Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return RedirectToAction("EditCaregiverAreas", "Account", new { id = id });
        }


        public ActionResult DeleteCitizenFingerprints(string id)
        {
            var Db = new ApplicationDbContext();
           

            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            foreach (var credential in credentials_list_figerprint)
            {
                Db.Credentials.Remove(credential);
                Db.SaveChanges();
            }


            return RedirectToAction("EditCitizenID", "Account", new { id = id });
        }

        public ActionResult DeleteCitizenFingerprintsAdmin(string id)
        {
            var Db = new ApplicationDbContext();


            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            foreach (var credential in credentials_list_figerprint)
            {
                Db.Credentials.Remove(credential);
                Db.SaveChanges();
            }


            return RedirectToAction("EditCitizenIDAdmin", "Account", new { id = id });
        }

        public ActionResult DeleteCaregiverFingerprints(string id)
        {
            var Db = new ApplicationDbContext();


            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            foreach (var credential in credentials_list_figerprint)
            {
                Db.Credentials.Remove(credential);
                Db.SaveChanges();
            }


            return RedirectToAction("EditCaregiverID", "Account", new { id = id });
        }

        public ActionResult DeleteCaregiverFingerprintsAdmin(string id)
        {
            var Db = new ApplicationDbContext();


            var credentials_list_figerprint = (from c in Db.Credentials
                                               where c.CarestoreUserId == id && c.CredentialMetric == CredentialMetricType.FINGERPRINT
                                               select c).ToList();


            foreach (var credential in credentials_list_figerprint)
            {
                Db.Credentials.Remove(credential);
                Db.SaveChanges();
            }


            return RedirectToAction("EditCaregiverIDAdmin", "Account", new { id = id });
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
      //  [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(string id)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            Db.Users.Remove(user);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        // [Authorize(Roles = "Admin")]
        public ActionResult DeleteCitizen(string id = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModel(user);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }


        [HttpPost, ActionName("DeleteCitizen")]
        [ValidateAntiForgeryToken]
        //  [Authorize(Roles = "Admin")]
        public ActionResult DeleteCitizenConfirmed(string id)
        {
            var Db = new ApplicationDbContext();


            var credentials2remove = from a in Db.Credentials select a;
            credentials2remove = credentials2remove.Where(a => a.CarestoreUserId == id);
            Db.Credentials.RemoveRange(credentials2remove);
            Db.SaveChanges();



            var user = Db.Users.First(u => u.Id == id);
            Db.Users.Remove(user);
            Db.SaveChanges();
            return RedirectToAction("CitizensList");
        }

        // [Authorize(Roles = "Admin")]
        public ActionResult DeleteCaregiver(string id = null)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new EditUserViewModel(user);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }


        [HttpPost, ActionName("DeleteCaregiver")]
        [ValidateAntiForgeryToken]
        //  [Authorize(Roles = "Admin")]
        public ActionResult DeleteCaregiverConfirmed(string id)
        {
            var Db = new ApplicationDbContext();

            var credentials2remove = from a in Db.Credentials select a;
            credentials2remove = credentials2remove.Where(a => a.CarestoreUserId == id);
            Db.Credentials.RemoveRange(credentials2remove);
            Db.SaveChanges();


            var user = Db.Users.First(u => u.Id == id);
            Db.Users.Remove(user);
            Db.SaveChanges();
            return RedirectToAction("CaregiversList");
        }

      //  [Authorize(Roles = "Admin")]
        public ActionResult UserRoles(string id)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First(u => u.Id == id);
            var model = new SelectUserRolesViewModel(user);
            return View(model);
        }


        [HttpPost]
     //   [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult UserRoles(SelectUserRolesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var idManager = new IdentityManager();

               // var store = new UserStore<ApplicationUser>(context);
                //var manager = new UserManager<ApplicationUser>(store);

                

               // manager.UserValidator = new UserValidator<ApplicationUser>(manager)
                //{
                  //  AllowOnlyAlphanumericUserNames = false
                //};



                var Db = new ApplicationDbContext();
                var user = Db.Users.First(u => u.Id == model.UserId);
                idManager.ClearUserRoles(user.Id);
                foreach (var role in model.Roles)
                {
                    if (role.Selected)
                    {
                       // idManager.AddUserToRole(user.Id, role.RoleName);
                        this.UserManager.AddToRole(user.Id, role.RoleName);
                    }
                }
                return RedirectToAction("index");
            }



            var errors = ModelState.Values.SelectMany(v => v.Errors);

            return View();
        }



      //  [Authorize(Roles = "Admin")]
        public ActionResult RegisterAdmin()
        {
            return View();
        }


        [HttpPost]
   //     [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterAdmin(RegisterAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = model.GetUser();
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Account");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //  [Authorize(Roles = "Admin")]
        public ActionResult RegisterCitizen()
        {
            RegisterAdminViewModel model = new RegisterAdminViewModel();

            model.UserName = "citizen@carestore.eu";
            model.Password = "123456";
            model.ConfirmPassword = "123456";

            return View(model);
        }


        [HttpPost]
        //     [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterCitizen(RegisterAdminViewModel model)
        {
            var Db = new ApplicationDbContext();

            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            Guid myGuid = Guid.NewGuid();


            model.UserName = myGuid.ToString() + "@carestore.eu";
            model.Password = myGuid.ToString();
            model.ConfirmPassword = model.Password;
            model.OrganizationRelationID = organization_id;





            if (ModelState.IsValid)
            {
               // var user = model.GetUser();

                var user = new User() { UserName = model.UserName, FirstName = model.FirstName, LastName=model.LastName, OrganizationRelationID=model.OrganizationRelationID  };
                 
                var result = await UserManager.CreateAsync(user, model.Password);



                if (result.Succeeded)
                {
                    //return RedirectToAction("Index", "Account");




                    this.UserManager.AddToRole(user.Id, "Citizen");

                    var userTemp = Db.Users.First(u => u.Id == user.Id);



                    Credential newNFC = new Credential();
                    newNFC.CarestoreUser = userTemp;
                    newNFC.CarestoreUserId = userTemp.Id;
                    newNFC.CredentialData = userTemp.Id;
                    newNFC.CredentialMetric = CredentialMetricType.NFC;
                   
                    Db.Credentials.Add(newNFC);
                    await  Db.SaveChangesAsync();

                 
                    return RedirectToAction("CitizensList", "Account");



                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //  [Authorize(Roles = "Admin")]
        public ActionResult RegisterCaregiver()
        {
            RegisterAdminViewModel model = new RegisterAdminViewModel();

            model.UserName = "caregiver@carestore.eu";
            model.Password = "123456";
            model.ConfirmPassword = "123456";

            return View(model);
        }


        [HttpPost]
        //     [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterCaregiver(RegisterAdminViewModel model)
        {
            var Db = new ApplicationDbContext();

            var user_id = User.Identity.GetUserId();

            int organization_id = Db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            Guid myGuid = Guid.NewGuid();


            model.UserName = myGuid.ToString() + "@carestore.eu";
            model.Password = myGuid.ToString();
            model.ConfirmPassword = model.Password;
            model.OrganizationRelationID = organization_id;





            if (ModelState.IsValid)
            {
                var user = model.GetUser();
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //return RedirectToAction("Index", "Account");




                    this.UserManager.AddToRole(user.Id, "Care Giver");

                    var userTemp = Db.Users.First(u => u.Id == user.Id);

                    Credential newNFC = new Credential();
                    newNFC.CarestoreUser = userTemp;
                    newNFC.CarestoreUserId = userTemp.Id;
                    newNFC.CredentialData = userTemp.Id;
                    newNFC.CredentialMetric = CredentialMetricType.NFC;

                    Db.Credentials.Add(newNFC);
                    await Db.SaveChangesAsync();


                    return RedirectToAction("CaregiversList", "Account");



                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        // GET: /Account/Manage
        public ActionResult ManageOther(ManageMessageId? message, String UserId)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPasswordOther(UserId);
            ViewBag.ReturnUrl = Url.Action("ManageOther",new { UserId = UserId });
            ViewBag.UserId = UserId;

            var user = UserManager.FindById(UserId);
         
            
            //user.

            ViewBag.UserFirstName = user.FirstName;
            ViewBag.UserLastName = user.LastName;

            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManageOther(ManageOtherUserViewModel model, String UserId)
        {
            bool hasPassword = HasPasswordOther(UserId);
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("ManageOther", new { UserId = UserId });
           

                if (ModelState.IsValid)
                {
                   // IdentityResult result = await UserManager.AddPasswordAsync(UserId, model.NewPassword);

                    ApplicationDbContext context = new ApplicationDbContext();                    
                    UserStore<User> store = new UserStore<User>(context);             
                    String hashedNewPassword =  UserManager.PasswordHasher.HashPassword(model.NewPassword);

                    User cUser = await store.FindByIdAsync(UserId);
                    await store.SetPasswordHashAsync(cUser, hashedNewPassword);
                    await store.UpdateAsync(cUser);

                    //if (result.Succeeded)
                   // {
                        return RedirectToAction("ManageOther", new { Message = ManageMessageId.SetPasswordSuccess, UserId = UserId });
                   // }
                   // else
                   // {
                    //    AddErrors(result);
                   // }
                }
            

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new User() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPasswordOther(String UserId)
        {
            var user = UserManager.FindById(UserId);
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}