﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CSMarketplace.Models;



namespace CSMarketplace.Controllers
{
    public class CarestoreController : ApiController
    {

        [Route("api/carestore/applications")]
        [HttpGet]
        public IEnumerable<applicationDetailsShort> GetAllApplications()
        {

          

            var Db = new ApplicationDbContext();
          
            IList<applicationDetailsShort> applicationsList = new List<applicationDetailsShort>();
            
            var applications = Db.Applications.ToList();
            foreach (var appData in applications)
            {
                applicationsList.Add(new applicationDetailsShort()
                {
                    ID = appData.ApplicationId,
                    NAME = appData.ApplicationName,
                    URL = appData.ApplicationUrl,
                    ICON = appData.ApplicationLogo
                });
            }

            return applicationsList;
        }

        [Route("api/carestore/applications/{id:int}")]
        [HttpGet]
        public applicationDetailsComplete GetApplication(int id)
        {
            //return db.Vendors;

            var Db = new ApplicationDbContext();

            Application application = Db.Applications.Find(id);

            applicationDetailsComplete applicationResponse = new applicationDetailsComplete();
            
            applicationResponse.ID = application.ApplicationId;
            applicationResponse.NAME = application.ApplicationName;
            applicationResponse.URL = application.ApplicationUrl;
            applicationResponse.ICON = application.ApplicationLogo;
            applicationResponse.DESCRIPTION =application.ApplicationDescription;

           

            var applicationScreens = Db.ApplicationScreens.Where(c => c.ApplicationID == id).OrderBy(s => s.ApplicationScreenSortIndex);


            List<string> myImagesCollection = new List<string>();

            foreach (var myScreen in applicationScreens)
            {
                myImagesCollection.Add(myScreen.ApplicationScreenUrl);
            }


            applicationResponse.IMAGES= myImagesCollection.ToArray();


            return applicationResponse;

        }


        [Route("api/carestore/drivers")]
        [HttpGet]
        public IEnumerable<driverDetailsShort> GetAllDrivers()
        {


            var Db = new ApplicationDbContext();

            IList<driverDetailsShort> driversList = new List<driverDetailsShort>();

            var drivers = Db.Drivers.ToList();
            foreach (var driverData in drivers)
            {
                driversList.Add(new driverDetailsShort()
                {
                    ID = driverData.DriverId,
                    NAME = driverData.DriverName,
                    URL = driverData.DriverUrl
                });
            }

            return driversList;
        }

        [Route("api/carestore/drivers/{id:int}")]
        [HttpGet]
        public driverDetailsComplete GetDriver(int id)
        {
            //return db.Vendors;

            var Db = new ApplicationDbContext();

            Driver driver = Db.Drivers.Find(id);

            driverDetailsComplete driverResponse = new driverDetailsComplete();

            driverResponse.ID = driver.DriverId;
            driverResponse.NAME = driver.DriverName;
            driverResponse.URL = driver.DriverUrl;
            driverResponse.DESCRIPTION = driver.DriverDescription;


            return driverResponse;

        }


        [Route("api/carestore/users/{PlatformId}")]
        [HttpGet]
        public IEnumerable<MarketplaceContract.User> GetAllUsers(String PlatformId)
        {


            var Db = new ApplicationDbContext();


            Guid myGuidStr = Guid.Parse(PlatformId);

            Platform platform = Db.Platforms.Where(p => p.PlatformGUID == myGuidStr).First();
            
           // .Find(PlatformId);

          //  .OrganizationId;

            var role_citizen = Db.Roles
                    .Where(b => b.Name == "Citizen")
                    .FirstOrDefault();

            var role_caregiver= Db.Roles
                    .Where(b => b.Name == "Care Giver")
                    .FirstOrDefault();

            IList<MarketplaceContract.User> usersList = new List<MarketplaceContract.User>();

            var citizens = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role_citizen.Id)).ToList();
            var caregivers = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role_caregiver.Id)).ToList();


            foreach (var citizenData in citizens)
            {
                if (citizenData.Platforms.Contains(platform))
                {
                        usersList.Add(new MarketplaceContract.User()
                        {
                            UserId = citizenData.Id
                        });

                        var query = from c in Db.Credentials
                                    where (c.CarestoreUserId == citizenData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                                    select c;


                        var templates = query.ToList();

                        List<string> myTemplatesCollection = new List<string>();

                        foreach (var myTemplate in templates)
                        {
                            myTemplatesCollection.Add(myTemplate.CredentialData);
                        }


                        usersList.Last().Templates = myTemplatesCollection.ToArray();
                }
            }

            foreach (var caregiverData in caregivers)
            {


                var caregiver_areas = caregiverData.Areas;
                Boolean assignedToPlatform = false;

                foreach (Area caregiver_area in caregiver_areas)
                {
                    if (caregiver_area.CarePlatforms.Contains(platform))
                    {
                        assignedToPlatform = true;                     
                        break;
                    }
                }

                if (assignedToPlatform)
                {
                    usersList.Add(new MarketplaceContract.User()
                    {
                        UserId = caregiverData.Id
                    });

                    var query = from c in Db.Credentials
                                where (c.CarestoreUserId == caregiverData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                                select c;


                    var templates = query.ToList();

                    List<string> myTemplatesCollection = new List<string>();

                    foreach (var myTemplate in templates)
                    {
                        myTemplatesCollection.Add(myTemplate.CredentialData);
                    }


                    usersList.Last().Templates = myTemplatesCollection.ToArray();
                }
            }



            return usersList;
        }


        [Route("api/carestore/citizens")]
        [HttpGet]
        public IEnumerable<MarketplaceContract.User> GetAllCitizens()
        {


            var Db = new ApplicationDbContext();


        
            var role = Db.Roles 
                    .Where(b => b.Name == "Citizen") 
                    .FirstOrDefault();

            IList<MarketplaceContract.User> citizensList = new List<MarketplaceContract.User>();

             var citizens = Db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();



             foreach (var citizenData in citizens)
            {
                citizensList.Add(new MarketplaceContract.User()
                {
                    UserId = citizenData.Id
                });


               

               // var templates = Db.Credentials.Where(c => c.CarestoreUserId == citizenData.Id).ToList();

                var query = from c in Db.Credentials
                            where (c.CarestoreUserId == citizenData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                            select c;


                var templates = query.ToList();

                List<string> myTemplatesCollection = new List<string>();

               foreach (var myTemplate in templates)
                {
                    myTemplatesCollection.Add(myTemplate.CredentialData);
                }


                citizensList.Last().Templates = myTemplatesCollection.ToArray();


                


            }

             return citizensList;
        }


        [Route("api/carestore/citizens/storetemplate")]
        [HttpPost]
        public HttpResponseMessage CitizenStoreTemplate(MarketplaceContract.User citizenTemplate)
        {

           // UserController
            //   api/users        /gets all
            //   api/users/user  /get profile
            //   api/users/user  /update profile (post)  

            //DriverController
            //   api/drivers

            /*  Example of posted raw JSON object
                {
                    "UserId": "511c01de-2807-4f00-80ea-14ca9a691818",
                    "Templates": ["aaaa","bbbb"]
                }
            */

            var Db = new ApplicationDbContext();

            var credentials2remove = from a in Db.Credentials select a;
            credentials2remove = credentials2remove.Where(a => a.CarestoreUserId == citizenTemplate.UserId);
            Db.Credentials.RemoveRange(credentials2remove);
            Db.SaveChanges(); 


            foreach (string myTemplate in citizenTemplate.Templates)
            {
                Credential newTemplate = new Credential();

                newTemplate.CarestoreUserId = citizenTemplate.UserId;
                newTemplate.CredentialMetric = CredentialMetricType.FINGERPRINT;
                newTemplate.CredentialData = myTemplate;

                Db.Credentials.Add(newTemplate);
                Db.SaveChanges();
            }



            

              

            return new HttpResponseMessage(HttpStatusCode.Created);

        }
        



    }


    public class applicationDetailsShort
    {

        public int ID { get; set; }
        public string NAME { get; set; }
        public string URL { get; set; }
        public string ICON { get; set; }

    }

    public class applicationDetailsComplete
    {

        public int ID { get; set; }
        public string NAME { get; set; }
        public string URL { get; set; }
        public string ICON { get; set; }
        public string DESCRIPTION { get; set; }
        public String[] IMAGES { get; set; }


    }

    public class driverDetailsShort
    {

        public int ID { get; set; }
        public string NAME { get; set; }
        public string URL { get; set; }

    }

    public class driverDetailsComplete
    {

        public int ID { get; set; }
        public string NAME { get; set; }
        public string URL { get; set; }
        public string DESCRIPTION { get; set; }

    }

    public class citizenDetailsShort
    {

        public string ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public String[] TEMPLATES { get; set; }

    }

    public class citizenFINGERPINTtemplate
    {

        public string ID { get; set; }
        public string TEMPLATE1 { get; set; }
        public string TEMPLATE2 { get; set; }

    }


}
