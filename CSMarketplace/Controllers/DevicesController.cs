﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Security.Claims;

namespace CSMarketplace.Controllers
{
    public class DevicesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Devices
        /*public async Task<ActionResult> Index()
        {
           // var devices = db.Devices.Include(d => d.Vendor);
           // return View(devices.ToList());

            return View(await db.Devices.Include(d => d.Vendor).ToListAsync());
        }*/
        public ActionResult Index(string sortOrder, string currentFilter, string search, int? page)
        {

            ViewBag.CurrentSort = sortOrder;

            if (page == null || page == 0)
            {

                page = 1;

            }


            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;


            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var devices = from d in db.Devices
                          select d;
            if (!String.IsNullOrEmpty(search))
            {
                devices = devices.Where(d => d.DeviceName.ToUpper().Contains(search.ToUpper())
                                       || d.Vendor.VendorName.ToUpper().Contains(search.ToUpper()));
            }
            else
            {

                devices = devices.Where(d => d.DeviceId > 0);

            }

            switch (sortOrder)
            {
                case "name_desc":
                    devices = devices.OrderByDescending(d => d.DeviceName);
                    ViewBag.NameSortParm = "name_asc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "name_asc":
                    devices = devices.OrderBy(d => d.DeviceName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "status_desc":
                    devices = devices.OrderByDescending(d => d.DeviceValidation);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_asc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "status_asc":
                    devices = devices.OrderBy(d => d.DeviceValidation);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "vendor_desc":
                    devices = devices.OrderByDescending(d => d.Vendor.VendorName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_asc";
                    break;
                case "vendor_asc":
                    devices = devices.OrderBy(d => d.Vendor.VendorName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;

                default:
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";

                    break;
            }

            ViewBag.SortOrder = sortOrder;

            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = devices.Count();

            int t_PagerNo = ViewBag.PagerNo;
            int t_PageID = ViewBag.PageID;
            int t_ItemsNo = ViewBag.ItemsNo;

            Decimal calculus = (Decimal)devices.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;

            return View(devices.ToList());
        }

        public ActionResult VendorIndex(string id)
        {

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == id).First().VendorId;

            var devices = from d in db.Devices select d;

            devices = devices.Where(a => a.VendorID == vendor_id);

            return View(devices.ToList());

        }

        // GET: /Applications/List
        public ActionResult List(int? cat, int? page)
        {

            var devices = from a in db.Devices select a;

            if (cat == null || cat == 0)
            {
                devices = devices.Where(a => a.DeviceValidation == DeviceValidationType.Validated && a.DeviceActive == true);

                ViewBag.currentCategoryIndex = 0;
            }
            else
            {
                devices = devices.Where(a => a.DeviceValidation == DeviceValidationType.Validated && a.DeviceActive == true && a.DeviceCateogryID == cat);
                ViewBag.currentCategoryIndex = cat;
            }


            if (page == null || page == 0)
            {

                page = 1;

            }




            var categories = from c in db.DeviceCategories select c;

            var CategoriesNames = new List<string> { };
            var CategoriesIndexes = new List<int> { };

            foreach (var item in categories)
            {
                CategoriesNames.Add(item.DeviceCategoryName);
                CategoriesIndexes.Add(item.DeviceCategoryId);
            }


            ViewData["CategoriesNames"] = CategoriesNames;
            ViewData["CategoriesIndexes"] = CategoriesIndexes;

            ViewBag.PagerNo = Properties.Settings.Default.PagerNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = devices.Count();

            Decimal calculus = devices.Count() / Properties.Settings.Default.PagerNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerNo;
              




            return View(devices.ToList());

            //  return View(db.Vendors.ToList());
        }



        // GET: Devices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }


            ViewBag.VendorName = db.Vendors.Find(device.VendorID).VendorName;



            var deviceScreens = db.DeviceScreens.Where(c => c.DeviceID == id).OrderBy(s => s.DeviceScreenSortIndex);

            var deviceImages = new List<string> { };

            foreach (var item in deviceScreens)
            {
                deviceImages.Add(item.DeviceScreenUrl);

            }

            ViewData["deviceImages"] = deviceImages;






            return View(device);
        }

        // GET: Devices/Create
        public ActionResult Create()
        {
            
            var user_id = User.Identity.GetUserId();

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == user_id).First().VendorId;

            ViewBag.vendor_id = vendor_id;

            return View();

        }

        // POST: Devices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DeviceId,DeviceName,DeviceDescription")] Device device)
        {

            var user_id = User.Identity.GetUserId();

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == user_id).First().VendorId;

            device.VendorID = vendor_id;
            device.DeviceActive = true;
            device.DeviceValidation = DeviceValidationType.Pending;

            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDevicesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "deviceicon_" + Server.UrlEncode(vendor_id.ToString() + device.DeviceName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            device.DeviceIcon = azureBlockBlob.Uri.ToString();
                        }
                    }
                    



                }
            }





            if (ModelState.IsValid)
            {
                db.Devices.Add(device);
                db.SaveChanges();

                return RedirectToAction("VendorIndex", "Devices", new { id = user_id });
            }

            return View(device);


        }

        // GET: Devices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }


            //  List<ApplicationCategory> ListItems = db.ApplicationCategories.ToList();
            //  SelectList Categories = new SelectList(ListItems);
            //  ViewData["Categories"] = Categories;

            ViewBag.Categories = new SelectList(db.DeviceCategories, "DeviceCategoryId", "DeviceCategoryName");

            return View(device);

        }

        public ActionResult EditVendor(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }


            //  List<ApplicationCategory> ListItems = db.ApplicationCategories.ToList();
            //  SelectList Categories = new SelectList(ListItems);
            //  ViewData["Categories"] = Categories;



            ViewBag.Categories = new SelectList(db.DeviceCategories, "DeviceCategoryId", "DeviceCategoryName");



            return View(device);
        }



        // POST: Devices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DeviceId,DeviceName,DeviceUrl,DeviceDescription,DeviceActive,DeviceValidation,DeviceIcon,DeviceSelf,VendorID,DeviceCateogryID")] Device device)
        {


            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDevicesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "deviceicon_" + Server.UrlEncode(device.VendorID.ToString() + device.DeviceName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            device.DeviceIcon = azureBlockBlob.Uri.ToString();
                        }
                    }
                    



                }
            }


            if (ModelState.IsValid)
            {
                db.Entry(device).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(device);



        }


        // POST: Devices/EditVendor/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVendor([Bind(Include = "DeviceId,DeviceName,DeviceUrl,DeviceDescription,DeviceActive,DeviceValidation,DeviceIcon,DeviceSelf,VendorID,DeviceCateogryID")] Device device)
        {
            var user_id = User.Identity.GetUserId();

            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDevicesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "deviceicon_" + Server.UrlEncode(device.VendorID.ToString() + device.DeviceName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            device.DeviceIcon = azureBlockBlob.Uri.ToString();
                        }
                    }




                }
            }


            if (ModelState.IsValid)
            {
                db.Entry(device).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("VendorIndex", "Devices", new { id = user_id });
            }
            return View(device);



        }




        // GET: Devices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // POST: Devices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Device device = db.Devices.Find(id);
            db.Devices.Remove(device);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
