﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Net.Mail;
using System.Net;



namespace CSMarketplace.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Message2 = "<b>Check this out</b>";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult SendMail(string SenderName, string SenderAddress, string Message, string Company, string Phone)
        {
            string username = Properties.Settings.Default.UserID;
            string password = Properties.Settings.Default.Password;
            NetworkCredential loginInfo = new NetworkCredential(username, password);
            MailMessage msg = new MailMessage();

            SmtpClient smtpClient = new SmtpClient(Properties.Settings.Default.SMTPServer, Properties.Settings.Default.SMTPPort);
            smtpClient.EnableSsl = Properties.Settings.Default.EnableSSL;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = loginInfo;

            string message = "Name: " + SenderName + "<br /> E-mail: " + SenderAddress + "<br /> Phone: " + Phone + "<br /> Company: " + Company + "<br /> Message: " + Message;

            try
            {
                msg.From = new MailAddress(Properties.Settings.Default.FromAddress, "Carestore Marketplace");
                msg.To.Add(new MailAddress(Properties.Settings.Default.ToAddress));
                msg.Subject = "CS Marketplace Form Message";
                msg.Body = message;
                msg.IsBodyHtml = true;

                smtpClient.Send(msg);
                return Content("Your message was sent successfully!");
            }
            catch (Exception)
            {
                return Content("There was an error... please try again.");
            }
        }
    }
}