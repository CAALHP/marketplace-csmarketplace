﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CSMarketplace.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CSMarketplace.Controllers
{
    public class UsersWebApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/UsersWebApi
        [Authorize(Roles = "CAALHP")] 
        [Route("api/users/{PlatformId}")]
        public async Task <IEnumerable<MarketplaceContract.User>> GetIdentityUsers(String PlatformId)
        {

            Guid myGuidStr = Guid.Parse(PlatformId);

            Platform platform = db.Platforms.Where(p => p.PlatformGUID == myGuidStr).First();

            var role_citizen = db.Roles
                    .Where(b => b.Name == "Citizen")
                    .FirstOrDefault();

            var role_caregiver = db.Roles
                    .Where(b => b.Name == "Care Giver")
                    .FirstOrDefault();

            IList<MarketplaceContract.User> usersList = new List<MarketplaceContract.User>();

            var citizens = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role_citizen.Id)).ToList();
            var caregivers = await db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role_caregiver.Id)).Include(y=>y.Areas).ToListAsync();


            foreach (var citizenData in citizens)
            {
                if (citizenData.Platforms.Contains(platform))
                {
                    usersList.Add(new MarketplaceContract.User()
                    {
                        UserId = citizenData.Id,
                        UserRole = MarketplaceContract.UserRoleType.Citizen
                    });

                    var query = from c in db.Credentials
                                where (c.CarestoreUserId == citizenData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                                select c;


                    var templates = query.ToList();

                    List<string> myTemplatesCollection = new List<string>();

                    foreach (var myTemplate in templates)
                    {
                        myTemplatesCollection.Add(myTemplate.CredentialData);
                    }


                    usersList.Last().Templates = myTemplatesCollection.ToArray();


                    var credentials_list_nfc_citizens = (from c in db.Credentials
                                                where c.CarestoreUserId == citizenData.Id && c.CredentialMetric == CredentialMetricType.NFC
                                                select c).ToList();

                    if (credentials_list_nfc_citizens.Count() > 0)
                    {
                        usersList.Last().NFC = credentials_list_nfc_citizens.First().CredentialData;
                    }else{

                        usersList.Last().NFC = "";
                    }


                }
            }

            foreach (var caregiverData in caregivers)
            {


                var caregiver_areas = caregiverData.Areas;

                List<Area> areas_containing_platform = new List<Area>();


                if(platform.CareAreaId != null){

                        Area node = platform.CareArea;
                        areas_containing_platform.Add(node);
                        Boolean loop_end = false;

                        do
                        {                      
                            if (node.AreaParentId == null)
                            {
                                loop_end = true;
                            }
                            else
                            {
                                
                                node = db.Areas.Find(node.AreaParentId);
                                areas_containing_platform.Add(node);
                            }
                        } while (loop_end == false);

                }


                Boolean assignedToPlatform = false;

                foreach (Area caregiver_temp_area in caregiver_areas)
                {
                    if (areas_containing_platform.Contains(caregiver_temp_area))
                    {
                        assignedToPlatform = true;
                        break;
                    }
                }








               // var caregiver_areas = db.Users.First(u => u.Id == caregiverData.Id).Areas;

               /* var caregiver_areas = caregiverData.Areas;
               
                foreach(Area caregiver_temp_area in caregiver_areas){

                    if (caregiver_temp_area.AreaParentId != null)
                    {

                        Area node = caregiver_temp_area;
                        Boolean loop_end = false;

                        do
                        {                      
                            if (node.AreaParentId == null)
                            {
                                loop_end = true;
                            }
                            else
                            {
                                
                                node = db.Areas.Find(node.AreaParentId);
                                caregiver_areas.Add(node);
                            }
                        } while (loop_end == false);

                    }                
                }
                
                Boolean assignedToPlatform = false;

                foreach (Area caregiver_area in caregiver_areas)
                {
                    if (caregiver_area.CarePlatforms.Contains(platform))
                    {
                        assignedToPlatform = true;
                        break;
                    }
                }*/

                if (assignedToPlatform)
                {
                    usersList.Add(new MarketplaceContract.User()
                    {
                        UserId = caregiverData.Id,
                        UserRole = MarketplaceContract.UserRoleType.Caregiver
                    });

                    var query = from c in db.Credentials
                                where (c.CarestoreUserId == caregiverData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                                select c;


                    var templates = query.ToList();

                    List<string> myTemplatesCollection = new List<string>();

                    foreach (var myTemplate in templates)
                    {
                        myTemplatesCollection.Add(myTemplate.CredentialData);
                    }


                    usersList.Last().Templates = myTemplatesCollection.ToArray();

                    var credentials_list_nfc_caregivers = (from c in db.Credentials
                                                           where c.CarestoreUserId == caregiverData.Id && c.CredentialMetric == CredentialMetricType.NFC
                                                         select c).ToList();

                    if (credentials_list_nfc_caregivers.Count() > 0)
                    {
                        usersList.Last().NFC = credentials_list_nfc_caregivers.First().CredentialData;
                    }
                    else
                    {

                        usersList.Last().NFC = "";
                    }


                }
            }



            return usersList;
        }


        // GET: api/UsersWebApi
        [Authorize(Roles = "Care Administrator")]
        [Route("api/users/organization/{ManagerId}")]
        public async Task<IEnumerable<MarketplaceContract.UserProfile>> GetIdentityOrganizationUsers(String ManagerId)
        {

            Guid myGuidStr = Guid.Parse(ManagerId);

            User user_db = db.Users.Find(ManagerId);
            if (user_db == null)
            {
                return null;
            }

            Organization organization_db = db.Organizations.First(x => x.AdministratorUserId == user_db.Id);
            if (organization_db == null)
            {
                return null;
            }

            

            var role_citizen = db.Roles
                    .Where(b => b.Name == "Citizen")
                    .FirstOrDefault();

            var role_caregiver = db.Roles
                    .Where(b => b.Name == "Care Giver")
                    .FirstOrDefault();

            IList<MarketplaceContract.UserProfile> usersList = new List<MarketplaceContract.UserProfile>();

            var citizens = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role_citizen.Id) && x.OrganizationRelationID == organization_db.OrganizationId).ToList();
            var caregivers = await db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role_caregiver.Id) && x.OrganizationRelationID == organization_db.OrganizationId).ToListAsync();


            foreach (var citizenData in citizens)
            {
                    usersList.Add(new MarketplaceContract.UserProfile()
                    {
                        UserId = citizenData.Id,
                        UserRole = MarketplaceContract.UserRoleType.Citizen,
                        FristName = citizenData.FirstName,
                        LastName = citizenData.LastName,
                        ProfilePhotoURL = citizenData.ProfilePicture
                    });

                    var query = from c in db.Credentials
                                where (c.CarestoreUserId == citizenData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                                select c;
                    var templates = query.ToList();

                    List<string> myTemplatesCollection = new List<string>();

                    foreach (var myTemplate in templates)
                    {
                        myTemplatesCollection.Add(myTemplate.CredentialData);
                    }

                    usersList.Last().Templates = myTemplatesCollection.ToArray();

                    var credentials_list_nfc_citizens = (from c in db.Credentials
                                                         where c.CarestoreUserId == citizenData.Id && c.CredentialMetric == CredentialMetricType.NFC
                                                         select c).ToList();
                    if (credentials_list_nfc_citizens.Count() > 0)
                    {
                        usersList.Last().NFC = credentials_list_nfc_citizens.First().CredentialData;
                    }
                    else
                    {

                        usersList.Last().NFC = "";
                    }           
            }

            foreach (var caregiverData in caregivers)
            {

                    usersList.Add(new MarketplaceContract.UserProfile()
                    {
                        UserId = caregiverData.Id,
                        UserRole = MarketplaceContract.UserRoleType.Caregiver,
                        FristName = caregiverData.FirstName,
                        LastName = caregiverData.LastName,
                        ProfilePhotoURL = caregiverData.ProfilePicture
                    });

                    var query = from c in db.Credentials
                                where (c.CarestoreUserId == caregiverData.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                                select c;


                    var templates = query.ToList();

                    List<string> myTemplatesCollection = new List<string>();

                    foreach (var myTemplate in templates)
                    {
                        myTemplatesCollection.Add(myTemplate.CredentialData);
                    }


                    usersList.Last().Templates = myTemplatesCollection.ToArray();

                    var credentials_list_nfc_caregivers = (from c in db.Credentials
                                                           where c.CarestoreUserId == caregiverData.Id && c.CredentialMetric == CredentialMetricType.NFC
                                                           select c).ToList();

                    if (credentials_list_nfc_caregivers.Count() > 0)
                    {
                        usersList.Last().NFC = credentials_list_nfc_caregivers.First().CredentialData;
                    }
                    else
                    {

                        usersList.Last().NFC = "";
                    }
            
            }

            return usersList;
        }

        // GET: api/users/5
        [Authorize(Roles = "CAALHP, Care Administrator")]
        [Route("api/users/profile/{id}")]
        [ResponseType(typeof(MarketplaceContract.UserProfile))]
        public IHttpActionResult GetUser(string id)
        {
            User user_db = db.Users.Find(id);
            if (user_db == null)
            {
                return NotFound();
            }


            MarketplaceContract.UserProfile user = new MarketplaceContract.UserProfile();
            user.UserId = user_db.Id;
            user.FristName = user_db.FirstName;
            user.LastName = user_db.LastName;
            user.ProfilePhotoURL = user_db.ProfilePicture;

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            
            string role_caregiver = "Care Giver";
            string role_citizen = "Citizen";


            var role_caregiver_id = db.Roles.Where(r => r.Name == role_caregiver).First().Id;
            var role_citizen_id = db.Roles.Where(r => r.Name == role_citizen).First().Id;
                 
               
            var roles = user_db.Roles;

            foreach(var role in roles){

                //if (role.Role.Name == "Citizen"){



                if (role.RoleId == role_citizen_id)
                {
                    user.UserRole = MarketplaceContract.UserRoleType.Citizen;
             //   } else if (role.Role.Name == "Care Giver"){
                }
                else if (role.RoleId == role_caregiver_id)
                {
                    user.UserRole = MarketplaceContract.UserRoleType.Caregiver;
                }


            }


            var query = from c in db.Credentials
                        where (c.CarestoreUserId == user_db.Id) && (c.CredentialMetric == CredentialMetricType.FINGERPRINT)
                        select c;


            var templates = query.ToList();

            List<string> myTemplatesCollection = new List<string>();

            foreach (var myTemplate in templates)
            {
                myTemplatesCollection.Add(myTemplate.CredentialData);
            }


            user.Templates = myTemplatesCollection.ToArray();

            var credentials_list_nfc = (from c in db.Credentials
                                                           where c.CarestoreUserId == user_db.Id && c.CredentialMetric == CredentialMetricType.NFC
                                                         select c).ToList();

                    if (credentials_list_nfc.Count() > 0)
                    {
                        user.NFC = credentials_list_nfc.First().CredentialData;
                    }
                    else
                    {

                        user.NFC = "";
                    }



          
            return Ok(user);
        }

        // PUT: api/users/
        [Authorize(Roles = "CAALHP, Care Administrator")]
        [Route("api/users")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(MarketplaceContract.UserProfile user)
        {

            string id = user.UserId;


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                return BadRequest();
            }
         
            var credentials2remove = from a in db.Credentials select a;
            credentials2remove = credentials2remove.Where(a => a.CarestoreUserId == user.UserId && a.CredentialMetric == CredentialMetricType.FINGERPRINT);
            db.Credentials.RemoveRange(credentials2remove);
            db.SaveChanges();


            foreach (string myTemplate in user.Templates)
            {
                Credential newTemplate = new Credential();

                newTemplate.CarestoreUserId = user.UserId;
                newTemplate.CredentialMetric = CredentialMetricType.FINGERPRINT;
                newTemplate.CredentialData = myTemplate;

                db.Credentials.Add(newTemplate);
                db.SaveChanges();
            }

            return StatusCode(HttpStatusCode.Created);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(string id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }


       

    }
}