﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Security.Claims;
 

namespace CSMarketplace.Controllers
{
    public class ApplicationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Applications/
        public ActionResult Index(string sortOrder, string currentFilter, string search, int? page)
        {

            ViewBag.CurrentSort = sortOrder;

            if (page == null || page == 0)
            {

                page = 1;

            }


            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;


            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }else{
                ViewBag.FilterWord = "Search inside table below...";
            }

            var applications = from a in db.Applications
                           select a;
            if (!String.IsNullOrEmpty(search))
            {
                applications = applications.Where(a => a.ApplicationName.ToUpper().Contains(search.ToUpper())
                                       || a.Vendor.VendorName.ToUpper().Contains(search.ToUpper()));
            }
            else
            {

                applications = applications.Where(a=>a.ApplicationId > 0);

            }

            switch (sortOrder)
            {
                case "name_desc":
                    applications = applications.OrderByDescending(a => a.ApplicationName);
                    ViewBag.NameSortParm = "name_asc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "name_asc":
                    applications = applications.OrderBy(a => a.ApplicationName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "status_desc":
                    applications = applications.OrderByDescending(a => a.ApplicationValidation);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_asc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "status_asc":
                    applications = applications.OrderBy(a => a.ApplicationValidation);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "vendor_desc":
                    applications = applications.OrderByDescending(a => a.Vendor.VendorName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_asc";
                    break;
                case "vendor_asc":
                    applications = applications.OrderBy(a => a.Vendor.VendorName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;

                default:
                      ViewBag.NameSortParm = "name_desc";
                      ViewBag.StatusSortParm = "status_desc";
                      ViewBag.VendorSortParm = "vendor_desc";
           
                    break;
            }

            ViewBag.SortOrder = sortOrder;

            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = applications.Count();

            Decimal calculus = (Decimal)applications.Count() / (Decimal) Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;
            
            return View(applications.ToList());
        }

        public ActionResult VendorIndex(string id)
        {

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == id).First().VendorId;

            var applications = from a in db.Applications select a;

            applications = applications.Where(a => a.VendorID == vendor_id);

            return View(applications.ToList());

        }

        // GET: /Applications/List
        public ActionResult List(int? cat,int? page)
        {
            var applications = from a in db.Applications select a;

            if (cat == null || cat == 0)
            {
                applications = applications.Where(a => a.ApplicationValidation == ApplicationValidationType.Validated && a.ApplicationActive == true);

                ViewBag.currentCategoryIndex = 0;
            }
            else
            {
                applications = applications.Where(a => a.ApplicationValidation == ApplicationValidationType.Validated && a.ApplicationActive == true && a.ApplicationCategoryID == cat);
                ViewBag.currentCategoryIndex = cat;
            }


            if (page == null || page == 0)
            {

                page = 1;

            }



            //applications = applications.Concat(applications);
         
            var categories = from c in db.ApplicationCategories select c;

            var CategoriesNames = new List<string> { };
            var CategoriesIndexes = new List<int> { }; 

             foreach(var item in categories){
                 if (item.ApplicationCategoryActive == true)
                 {
                     CategoriesNames.Add(item.ApplicationCategoryName);
                     CategoriesIndexes.Add(item.ApplicationCategoryId);
                 }
                 
             }


             ViewData["CategoriesNames"] = CategoriesNames;
             ViewData["CategoriesIndexes"] = CategoriesIndexes;

             ViewBag.PagerNo = Properties.Settings.Default.PagerNo;
             ViewBag.PageID = page;
             ViewBag.ItemsNo = applications.Count();

             Decimal calculus = applications.Count() / Properties.Settings.Default.PagerNo;
             calculus=Math.Ceiling(calculus);
             int pagesqty = Convert.ToInt32(calculus); ;
             ViewBag.PagesQty = pagesqty;
              
              if (page == 1) {

                  ViewBag.PagePrevIndex = 1;
                  ViewBag.PageNextIndex = 2;

              }
              else if (page == pagesqty)
              {
                  ViewBag.PagePrevIndex = pagesqty-1;
                  ViewBag.PageNextIndex = pagesqty;

              }
              else
              {
                  ViewBag.PagePrevIndex = page - 1;
                  ViewBag.PageNextIndex = page + 1;

              }

              ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerNo;
              



            return View(applications.ToList());

            //  return View(db.Vendors.ToList());
        }



        // GET: /Applications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }




            ViewBag.VendorName = db.Vendors.Find(application.VendorID).VendorName;



            var applicationScreens = db.ApplicationScreens.Where(c => c.ApplicationID == id).OrderBy(s => s.ApplicationScreenSortIndex);

            var appImages = new List<string> { };

            foreach (var item in applicationScreens)
            {
                appImages.Add(item.ApplicationScreenUrl);
               
            }

            ViewData["appImages"] = appImages;
           


            return View(application);
        }

        // GET: /Applications/View/5
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }




            ViewBag.VendorName = db.Vendors.Find(application.VendorID).VendorName;



            var applicationScreens = db.ApplicationScreens.Where(c => c.ApplicationID == id).OrderBy(s => s.ApplicationScreenSortIndex);

            var appImages = new List<string> { };

            foreach (var item in applicationScreens)
            {
                appImages.Add(item.ApplicationScreenUrl);

            }

            ViewData["appImages"] = appImages;



            return View(application);
        }

        // GET: /Applications/Create
        public ActionResult Create()
        {
            var user_id = User.Identity.GetUserId();

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == user_id).First().VendorId;

            ViewBag.vendor_id=vendor_id;

            return View();
        }

        // POST: /Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationId,ApplicationName,ApplicationDescription")] Application application)
        {
            var user_id = User.Identity.GetUserId();

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == user_id).First().VendorId;

            application.VendorID = vendor_id;
            application.ApplicationActive = true;
            application.ApplicationValidation = ApplicationValidationType.Pending;

            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceApplicationsData);
                var storageContainerImages = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceImagesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "applogo_" + Server.UrlEncode(vendor_id.ToString() + application.ApplicationName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                         if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                         {
                                azureBlockBlob = storageContainerImages.GetBlockBlobReference(fileName);
                                azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                                application.ApplicationLogo = azureBlockBlob.Uri.ToString();
                          }
                    }
                    else if (file_source == "FileUploader2")
                    {
                        fileName = "app_" + Server.UrlEncode(vendor_id.ToString() + application.ApplicationName) + DateTime.Now.ToString("yyyyMMddHHmmss")+".zip";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            application.ApplicationUrl = azureBlockBlob.Uri.ToString();
                        }


                    }


                    
                }
            }





            if (ModelState.IsValid)
            {
                db.Applications.Add(application);
                db.SaveChanges();
                
                return RedirectToAction("VendorIndex", "Applications", new { id = user_id });
            }

            return View(application);
        }

        // GET: /Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }


          //  List<ApplicationCategory> ListItems = db.ApplicationCategories.ToList();
          //  SelectList Categories = new SelectList(ListItems);
          //  ViewData["Categories"] = Categories;


            
            ViewBag.Categories = new SelectList(db.ApplicationCategories, "ApplicationCategoryId", "ApplicationCategoryName");



            return View(application);
        }

        // GET: /Applications/Edit/5
        public ActionResult EditVendor(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }


            //  List<ApplicationCategory> ListItems = db.ApplicationCategories.ToList();
            //  SelectList Categories = new SelectList(ListItems);
            //  ViewData["Categories"] = Categories;



            ViewBag.Categories = new SelectList(db.ApplicationCategories, "ApplicationCategoryId", "ApplicationCategoryName");



            return View(application);
        }

        // POST: /Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationId,ApplicationName,ApplicationUrl,ApplicationSelf,ApplicationDescription,ApplicationActive,ApplicationValidation,ApplicationLogo,VendorID,ApplicationCategoryID")] Application application)
        {

            

            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceApplicationsData);
                var storageContainerImages = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceImagesData);
                

                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "applogo_" + Server.UrlEncode(application.VendorID.ToString() + application.ApplicationName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainerImages.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            application.ApplicationLogo = azureBlockBlob.Uri.ToString();
                        }
                    }
                    else if (file_source == "FileUploader2")
                    {
                        fileName = "app_" + Server.UrlEncode(application.VendorID.ToString() + application.ApplicationName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            application.ApplicationUrl = azureBlockBlob.Uri.ToString();
                        }


                    }



                }
            }


            if (ModelState.IsValid)
            {
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(application);
        }


        // POST: /Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVendor([Bind(Include = "ApplicationId,ApplicationName,ApplicationUrl,ApplicationSelf,ApplicationDescription,ApplicationActive,ApplicationValidation,ApplicationLogo,VendorID,ApplicationCategoryID")] Application application)
        {

            var user_id = User.Identity.GetUserId();

            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceApplicationsData);
                var storageContainerImages = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceApplicationsData);
                

                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "applogo_" + Server.UrlEncode(application.VendorID.ToString() + application.ApplicationName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainerImages.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            application.ApplicationLogo = azureBlockBlob.Uri.ToString();
                        }
                    }
                    else if (file_source == "FileUploader2")
                    {
                        fileName = "app_" + Server.UrlEncode(application.VendorID.ToString() + application.ApplicationName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            application.ApplicationUrl = azureBlockBlob.Uri.ToString();
                        }


                    }



                }
            }


            if (ModelState.IsValid)
            {
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("VendorIndex", "Applications", new { id = user_id });
            }
            return View(application);
        }

        // GET: /Applications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: /Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
