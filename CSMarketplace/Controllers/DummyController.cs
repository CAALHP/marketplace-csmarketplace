﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

using System.Web.Security;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using CSMarketplace.Models;
using System.Data.Entity.Validation;

using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Owin.Host.SystemWeb;

namespace CSMarketplace.Controllers
{
    public class DummyController : ApiController
    {

        // GET: api/dummy/say
        [Route("api/dummy/say/{word}")]
        [HttpGet]
        public String SaySomething(String word)
        {           
            return word;
        }

        // GET: api/dummy/manager
        [Route("api/dummy/manager")]
        [HttpGet]
        public MarketplaceContract.UserCredentialsData GetDummy()
        {
            MarketplaceContract.UserCredentialsData UserData = new MarketplaceContract.UserCredentialsData();

            UserData.UserLogin = "manager@carestore.eu";
            UserData.UserPassword = "123456";
            return UserData;
        }







    }
}
