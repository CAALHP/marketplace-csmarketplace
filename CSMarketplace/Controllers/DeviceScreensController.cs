﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Security.Claims;
using CSMarketplace.Models;

namespace CSMarketplace.Controllers
{
    public class DeviceScreensController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DeviceScreens
        public ActionResult Index(int? device_id)
        {

            var deviceScreens = db.DeviceScreens.Where(c => c.DeviceID == device_id).OrderBy(s => s.DeviceScreenSortIndex);
            ViewBag.DeviceID = device_id;
            //Include(a => a.Application)
            //
            return View(deviceScreens.ToList());

        }

        // GET: DeviceScreens
        public ActionResult VendorIndex(int? device_id)
        {

            var deviceScreens = db.DeviceScreens.Where(c => c.DeviceID == device_id).OrderBy(s => s.DeviceScreenSortIndex);
            ViewBag.DeviceID = device_id;
            //Include(a => a.Application)
            //
            return View(deviceScreens.ToList());

        }

        // GET: DeviceScreens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceScreen deviceScreen = db.DeviceScreens.Find(id);
            if (deviceScreen == null)
            {
                return HttpNotFound();
            }
            return View(deviceScreen);
        }

        // GET: DeviceScreens/Create
        public ActionResult Create(int? device_id)
        {
           // ViewBag.DeviceID = new SelectList(db.Devices, "DeviceId", "DeviceName");
           // return View();

            ViewBag.DeviceID = device_id;
            return View();
        }

        // POST: DeviceScreens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int device_id, [Bind(Include = "DeviceScreenId,DeviceScreenSortIndex,DeviceID")] DeviceScreen deviceScreen)
        {

            Boolean status_ok = false;



            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0
            Device device = db.Devices.Find(device_id);
            deviceScreen.DeviceScreenUrl = "problem";
            deviceScreen.DeviceID = device_id;
            deviceScreen.Device = device;

            if (deviceScreen.DeviceScreenSortIndex < 1)
            {

                deviceScreen.DeviceScreenSortIndex = 1;

            }



            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDevicesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "devicescreen_" + Server.UrlEncode(deviceScreen.DeviceID.ToString()) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            deviceScreen.DeviceScreenUrl = azureBlockBlob.Uri.ToString();

                            status_ok = true;



                        }
                    }




                }
            }



            if (status_ok)
            {
                db.DeviceScreens.Add(deviceScreen);
                db.SaveChanges();

                return RedirectToAction("Index", "DeviceScreens", new { device_id = device_id });
            }

            ViewBag.DeviceID = deviceScreen.DeviceID;

            // ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationId", "ApplicationName", applicationScreen.ApplicationID);
            return View(deviceScreen);


        }


       // GET: DeviceScreens/Create
        public ActionResult VendorCreate(int? device_id)
        {
           // ViewBag.DeviceID = new SelectList(db.Devices, "DeviceId", "DeviceName");
           // return View();

            ViewBag.DeviceID = device_id;
            return View();
        }


        // POST: DeviceScreens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VendorCreate(int device_id, [Bind(Include = "DeviceScreenId,DeviceScreenSortIndex,DeviceID")] DeviceScreen deviceScreen)
        {

            Boolean status_ok = false;



            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0
            Device device = db.Devices.Find(device_id);
            deviceScreen.DeviceScreenUrl = "problem";
            deviceScreen.DeviceID = device_id;
            deviceScreen.Device = device;

            if (deviceScreen.DeviceScreenSortIndex < 1)
            {

                deviceScreen.DeviceScreenSortIndex = 1;

            }



            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDevicesData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader")
                    {
                        fileName = "devicescreen_" + Server.UrlEncode(deviceScreen.DeviceID.ToString()) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            deviceScreen.DeviceScreenUrl = azureBlockBlob.Uri.ToString();

                            status_ok = true;



                        }
                    }




                }
            }



            if (status_ok)
            {
                db.DeviceScreens.Add(deviceScreen);
                db.SaveChanges();

                return RedirectToAction("VendorIndex", "DeviceScreens", new { device_id = device_id });
            }

            ViewBag.DeviceID = deviceScreen.DeviceID;

            // ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationId", "ApplicationName", applicationScreen.ApplicationID);
            return View(deviceScreen);


        }

        // GET: DeviceScreens/Edit/5
        public ActionResult Edit(int ds_id, int device_id)
        {

            if (ds_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceScreen deviceScreen = db.DeviceScreens.Find(ds_id);
            if (deviceScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceID = device_id;
            return View(deviceScreen);


        }

        // POST: DeviceScreens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int device_id, [Bind(Include = "DeviceScreenId,DeviceScreenUrl,DeviceScreenSortIndex,DeviceID")] DeviceScreen deviceScreen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deviceScreen).State = EntityState.Modified;
                db.SaveChanges();
               // return RedirectToAction("Index");
                return RedirectToAction("Index", "DeviceScreens", new { device_id = device_id });
            }
            ViewBag.DeviceID = device_id;
            return View(deviceScreen);
        }


        // GET: DeviceScreens/Edit/5
        public ActionResult VendorEdit(int ds_id, int device_id)
        {

            if (ds_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceScreen deviceScreen = db.DeviceScreens.Find(ds_id);
            if (deviceScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceID = device_id;
            return View(deviceScreen);


        }

        // POST: DeviceScreens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VendorEdit(int device_id, [Bind(Include = "DeviceScreenId,DeviceScreenUrl,DeviceScreenSortIndex,DeviceID")] DeviceScreen deviceScreen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deviceScreen).State = EntityState.Modified;
                db.SaveChanges();
                // return RedirectToAction("Index");
                return RedirectToAction("VendorIndex", "DeviceScreens", new { device_id = device_id });
            }
            ViewBag.DeviceID = device_id;
            return View(deviceScreen);
        }


        // GET: DeviceScreens/Delete/5
        public ActionResult Delete(int ds_id, int device_id)
        {
            if (ds_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceScreen deviceScreen = db.DeviceScreens.Find(ds_id);
            if (deviceScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceID = device_id;
            return View(deviceScreen);
        }

        // POST: DeviceScreens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int ds_id, int device_id)
        {
            DeviceScreen deviceScreen = db.DeviceScreens.Find(ds_id);
            db.DeviceScreens.Remove(deviceScreen);
            db.SaveChanges();
            return RedirectToAction("Index", "DeviceScreens", new { device_id = device_id });
        }

        // GET: DeviceScreens/Delete/5
        public ActionResult VendorDelete(int ds_id, int device_id)
        {
            if (ds_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceScreen deviceScreen = db.DeviceScreens.Find(ds_id);
            if (deviceScreen == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceID = device_id;
            return View(deviceScreen);
        }

        // POST: DeviceScreens/Delete/5
        [HttpPost, ActionName("VendorDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult VendorDeleteConfirmed(int ds_id, int device_id)
        {
            DeviceScreen deviceScreen = db.DeviceScreens.Find(ds_id);
            db.DeviceScreens.Remove(deviceScreen);
            db.SaveChanges();
            return RedirectToAction("VendorIndex", "DeviceScreens", new { device_id = device_id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
