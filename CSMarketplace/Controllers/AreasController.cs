﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;
using Microsoft.AspNet.Identity;

namespace CSMarketplace.Controllers
{
    public class AreasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Areas
        public ActionResult Index()
        {

            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;


            AreaTree AreasTree = new AreaTree();
            
                // Add each element as a tree node

            var areas = db.Areas.Where(a => a.OrganizationId == organization_id).OrderBy(a => a.AreaSortIndex);
            List<AreaNode> areas_nodes=new List<AreaNode>();


            foreach(var area in areas){

                AreaNode area_node = new AreaNode();
                area_node.AreaId = area.AreaId;
                area_node.AreaName = area.AreaName;
                area_node.AreaSortIndex = area.AreaSortIndex;
                area_node.AreaParentId = area.AreaParentId;
                area_node.AreaParent = area.AreaParent;

                areas_nodes.Add(area_node);
            }

            AreasTree.Nodes = areas_nodes.ToDictionary(t => t.AreaId);

                // Create a new root node
            AreasTree.RootNode = new AreaNode { AreaId = 0, AreaName = "Root" };

                // Build the tree, setting parent and children references for all elements
            AreasTree.BuildAreaTree();

          //  ViewBag.AreasTree = AreasTree;

           // return View(db.Areas.Where(a => a.OrganizationId == organization_id).ToList());
            return View(AreasTree);
        }

        // GET: Areas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Areas.Find(id);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        // GET: Areas/Create
        public ActionResult Create()
        {
            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;
            
            return View();
        }

        // POST: Areas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AreaId,AreaName,AreaAddress,AreaSortIndex,AreaParentId,OrganizationId")] Area area)
        {

            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;

            area.OrganizationId = organization_id;

            

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;


            if (ModelState.IsValid)
            {
                db.Areas.Add(area);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(area);
        }

        // GET: Areas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Areas.Find(id);
            if (area == null)
            {
                return HttpNotFound();
            }

            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;



            return View(area);
        }

        // POST: Areas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AreaId,AreaName,AreaAddress,AreaSortIndex,AreaParentId,OrganizationId")] Area area)
        {

            var user_id = User.Identity.GetUserId();

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == user_id).First().OrganizationId;

            ViewBag.organization_id = organization_id;

            area.OrganizationId = organization_id;

            if (ModelState.IsValid)
            {
                db.Entry(area).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            

            SelectList areas_list = new SelectList(db.Areas.Where(a => a.OrganizationId == organization_id), "AreaId", "AreaName");

            ViewBag.Areas = areas_list;

            return View(area);
        }

        // GET: Areas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Areas.Find(id);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        // POST: Areas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Area area = db.Areas.Find(id);
            db.Areas.Remove(area);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
