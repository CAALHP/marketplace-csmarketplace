﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;

namespace CSMarketplace.Controllers
{
    public class DeviceEntitiesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DeviceEntities
        public async Task<ActionResult> Index(int? device_id, int? error)
        {
            if (error == null || error == 0)
            {

                error = 0;

            }

            var deviceEntities = db.DeviceEntities.Include(d => d.Device).Where(c => c.DeviceID == device_id);
            ViewBag.DeviceID = device_id;
            ViewBag.Error = error;
            return View(await deviceEntities.ToListAsync());
        }

        // GET: DeviceEntities
        /*public async Task<ActionResult> AllIndex()
        {
            
            var deviceEntities = db.DeviceEntities.Include(d => d.Device).Where(c => c.DeviceID > 0);
            return View(await deviceEntities.ToListAsync());
        }*/


        public async Task<ActionResult> AllIndex(string currentFilter, string search, int? page)
        {

             

            if (page == null || page == 0)
            {

                page = 1;

            }


            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;


            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var deviceEntities = from de in db.DeviceEntities.Include(d => d.Device)
                               select de;
            if (!String.IsNullOrEmpty(search))
            {
                deviceEntities = deviceEntities.Where(de => de.DeviceEntityNFC.ToUpper().Contains(search.ToUpper())
                                       || de.DeviceEntityMAC.ToUpper().Contains(search.ToUpper())|| de.Device.DeviceName.ToUpper().Contains(search.ToUpper()));
            }
            else
            {

                deviceEntities = deviceEntities.Where(de => de.DeviceEntityId > 0);

            }

            
            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = deviceEntities.Count();

            Decimal calculus = (Decimal)deviceEntities.Count() / (Decimal)Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;

            //return View(applications.ToList());
            return View(await deviceEntities.ToListAsync());
        }












        // GET: DeviceEntities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(id);
            if (deviceEntity == null)
            {
                return HttpNotFound();
            }
            return View(deviceEntity);
        }

        // GET: DeviceEntities/Create
        public ActionResult Create()
        {
            ViewBag.DeviceID = new SelectList(db.Devices, "DeviceId", "DeviceName");
            return View();
        }

        // POST: DeviceEntities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DeviceEntityId,DeviceEntityName,DeviceEntityMAC,DeviceEntityPairingCode,DeviceEntityNFC,OrganizationId,DeviceID")] DeviceEntity deviceEntity)
        {
            if (ModelState.IsValid)
            {
                db.DeviceEntities.Add(deviceEntity);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DeviceID = new SelectList(db.Devices, "DeviceId", "DeviceName", deviceEntity.DeviceID);
            return View(deviceEntity);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddNFC(int device_id, string NFCCode, string MACCode, string PAIRCode)
        {

            Boolean validate = true;
            Int32 error_code = 0;



            if (!String.IsNullOrEmpty(NFCCode) || !String.IsNullOrEmpty(MACCode))
            {

                if (!String.IsNullOrEmpty(NFCCode))
                {

                    var deviceEntities = db.DeviceEntities.Where(c => c.DeviceEntityNFC == NFCCode);

                    if (deviceEntities.Count() > 0)
                    {
                        validate = false;
                        error_code = 2;

                    }

                }

                if (!String.IsNullOrEmpty(MACCode))
                {

                    var deviceEntities2 = db.DeviceEntities.Where(c => c.DeviceEntityMAC == MACCode);

                    if (deviceEntities2.Count() > 0)
                    {
                        validate = false;
                        error_code = 3;

                    }

                }

            }
            else
            {

                validate = false;
                error_code = 1;
            }

            if (validate == true)
            {
        
               DeviceEntity deviceEntity = new DeviceEntity();

               deviceEntity.DeviceID = device_id;
               deviceEntity.DeviceEntityNFC=NFCCode;
               deviceEntity.DeviceEntityMAC = MACCode;
               deviceEntity.DeviceEntityPairingCode = PAIRCode;

               db.DeviceEntities.Add(deviceEntity);
               await db.SaveChangesAsync();

            } 

            return RedirectToAction("Index", "DeviceEntities", new { device_id = device_id, error=error_code });
        }

        // GET: DeviceEntities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(id);
            if (deviceEntity == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceID = new SelectList(db.Devices, "DeviceId", "DeviceName", deviceEntity.DeviceID);
            return View(deviceEntity);
        }

        // POST: DeviceEntities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DeviceEntityId,DeviceEntityName,DeviceEntityMAC,DeviceEntityPairingCode,DeviceEntityNFC,OrganizationId,DeviceID")] DeviceEntity deviceEntity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deviceEntity).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DeviceID = new SelectList(db.Devices, "DeviceId", "DeviceName", deviceEntity.DeviceID);
            return View(deviceEntity);
        }

        // GET: DeviceEntities/Delete/5
        public async Task<ActionResult> Delete(int de_id, int device_id)
        {
            if (de_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            /*DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            if (deviceEntity == null)
            {
                return HttpNotFound();
            }
            return View(deviceEntity);*/

            DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            db.DeviceEntities.Remove(deviceEntity);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "DeviceEntities", new { device_id = device_id });

        }

        // GET: DeviceEntities/Delete/5
        public async Task<ActionResult> AllDelete(int de_id)
        {
            if (de_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            /*DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            if (deviceEntity == null)
            {
                return HttpNotFound();
            }
            return View(deviceEntity);*/

            DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            db.DeviceEntities.Remove(deviceEntity);
            await db.SaveChangesAsync();
            return RedirectToAction("AllIndex", "DeviceEntities", null);

        }

        // POST: DeviceEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int de_id, int device_id)
        {
            DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            db.DeviceEntities.Remove(deviceEntity);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "DeviceEntities", new { device_id = device_id });


        }

        // POST: DeviceEntities/Delete/5
        [HttpPost, ActionName("AllDelete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AllDeleteConfirmed(int de_id)
        {
            DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            db.DeviceEntities.Remove(deviceEntity);
            await db.SaveChangesAsync();
            return RedirectToAction("AllIndex", "DeviceEntities", null);


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
