﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;

namespace CSMarketplace.Controllers
{
    public class DeviceCategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DeviceCategories
        public ActionResult Index()
        {
            return View(db.DeviceCategories.ToList());
        }

        // GET: DeviceCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceCategory deviceCategory = db.DeviceCategories.Find(id);
            if (deviceCategory == null)
            {
                return HttpNotFound();
            }
            return View(deviceCategory);
        }

        // GET: DeviceCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DeviceCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DeviceCategoryId,DeviceCategoryName,DeviceCategoryActive,DeviceCategoryIcon")] DeviceCategory deviceCategory)
        {
            if (ModelState.IsValid)
            {
                db.DeviceCategories.Add(deviceCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(deviceCategory);
        }

        // GET: DeviceCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceCategory deviceCategory = db.DeviceCategories.Find(id);
            if (deviceCategory == null)
            {
                return HttpNotFound();
            }
            return View(deviceCategory);
        }

        // POST: DeviceCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DeviceCategoryId,DeviceCategoryName,DeviceCategoryActive,DeviceCategoryIcon")] DeviceCategory deviceCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deviceCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(deviceCategory);
        }

        // GET: DeviceCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceCategory deviceCategory = db.DeviceCategories.Find(id);
            if (deviceCategory == null)
            {
                return HttpNotFound();
            }
            return View(deviceCategory);
        }

        // POST: DeviceCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DeviceCategory deviceCategory = db.DeviceCategories.Find(id);

            db.Database.ExecuteSqlCommand("UPDATE Devices SET DeviceCateogryID = null WHERE DeviceCateogryID={0}", id);

            db.DeviceCategories.Remove(deviceCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
