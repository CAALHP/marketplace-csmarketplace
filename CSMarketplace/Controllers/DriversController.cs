﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Security.Claims;

namespace CSMarketplace.Controllers
{
    public class DriversController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Drivers/
       /* public async Task<ActionResult> Index()
        {
           // return View(db.Drivers.ToList());


            return View(await db.Drivers.Include(c => c.Vendor).ToListAsync());
        } */
        public ActionResult Index(string sortOrder, string currentFilter, string search, int? page)
        {

            ViewBag.CurrentSort = sortOrder;

            if (page == null || page == 0)
            {

                page = 1;

            }


            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;


            if (!String.IsNullOrEmpty(search))
            {
                ViewBag.FilterWord = search;
            }
            else
            {
                ViewBag.FilterWord = "Search inside table below...";
            }

            var drivers = from d in db.Drivers
                               select d;
            if (!String.IsNullOrEmpty(search))
            {
                drivers = drivers.Where(d => d.DriverName.ToUpper().Contains(search.ToUpper())
                                       || d.Vendor.VendorName.ToUpper().Contains(search.ToUpper()));
            }
            else
            {

                drivers = drivers.Where(d => d.DriverId > 0);

            }

            switch (sortOrder)
            {
                case "name_desc":
                    drivers = drivers.OrderByDescending(d => d.DriverName);
                    ViewBag.NameSortParm = "name_asc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "name_asc":
                    drivers = drivers.OrderBy(d => d.DriverName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "status_desc":
                    drivers = drivers.OrderByDescending(d => d.DriverValidation);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_asc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "status_asc":
                    drivers = drivers.OrderBy(d => d.DriverValidation);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;
                case "vendor_desc":
                    drivers = drivers.OrderByDescending(d => d.Vendor.VendorName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_asc";
                    break;
                case "vendor_asc":
                    drivers = drivers.OrderBy(d => d.Vendor.VendorName);
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";
                    break;

                default:
                    ViewBag.NameSortParm = "name_desc";
                    ViewBag.StatusSortParm = "status_desc";
                    ViewBag.VendorSortParm = "vendor_desc";

                    break;
            }

            ViewBag.SortOrder = sortOrder;

            ViewBag.PagerNo = Properties.Settings.Default.PagerAdminNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = drivers.Count();

            int t_PagerNo = ViewBag.PagerNo;
            int t_PageID = ViewBag.PageID;
            int t_ItemsNo = ViewBag.ItemsNo;

            Decimal calculus = (Decimal)drivers.Count() / (Decimal) Properties.Settings.Default.PagerAdminNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerAdminNo;

            return View(drivers.ToList());
        }

        public ActionResult VendorIndex(string id)
        {

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == id).First().VendorId;

            var drivers = from a in db.Drivers select a;

            drivers = drivers.Where(d => d.VendorID == vendor_id);

            return View(drivers.ToList());

        }

        public ActionResult DevicesList(int driver_id)
        {

            var all_devices = db.Devices.Where(d => d.DeviceValidation == DeviceValidationType.Validated);

            ViewBag.AllDevices = new SelectList(all_devices, "DeviceId", "DeviceName");

            var devices = db.Drivers.Where(d => d.DriverId == driver_id).SelectMany(drivers => drivers.Devices);

            ViewBag.DriverId = driver_id;

            return View(devices.ToList());

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDevice(int driver_id, string AllDevices)
        {

            int device_id=  Convert.ToInt32(AllDevices);

            Device device = db.Devices.Find(device_id);
            Driver driver = db.Drivers.Find(driver_id);

            driver.Devices.Add(device);
            db.Entry(driver).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("DevicesList", "Drivers", new { driver_id = driver_id });
        }

        // GET: Drivers/DeleteDevice/5
        public ActionResult DeleteDevice(int driver_id, int device_id)
        {
            if (driver_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            /*DeviceEntity deviceEntity = await db.DeviceEntities.FindAsync(de_id);
            if (deviceEntity == null)
            {
                return HttpNotFound();
            }
            return View(deviceEntity);*/

            Driver driver = db.Drivers.Find(driver_id);
            Device device = db.Devices.Find(device_id);

            driver.Devices.Remove(device);

            db.Entry(driver).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("DevicesList", "Drivers", new { driver_id = driver_id });

        }


        // GET: /Drivers/List
        public ActionResult List(int? page)
        {
            if (page == null || page == 0)
            {

                page = 1;

            }



            var drivers = from d in db.Drivers select d;

            drivers = drivers.Where(d => d.DriverValidation == DriverValidationType.Validated && d.DriverActive == true);

            

            ViewBag.PagerNo = Properties.Settings.Default.PagerNo;
            ViewBag.PageID = page;
            ViewBag.ItemsNo = drivers.Count();

            Decimal calculus = drivers.Count() / Properties.Settings.Default.PagerNo;
            calculus = Math.Ceiling(calculus);
            int pagesqty = Convert.ToInt32(calculus); ;
            ViewBag.PagesQty = pagesqty;

            if (page == 1)
            {

                ViewBag.PagePrevIndex = 1;
                ViewBag.PageNextIndex = 2;

            }
            else if (page == pagesqty)
            {
                ViewBag.PagePrevIndex = pagesqty - 1;
                ViewBag.PageNextIndex = pagesqty;

            }
            else
            {
                ViewBag.PagePrevIndex = page - 1;
                ViewBag.PageNextIndex = page + 1;

            }

            ViewBag.ItemStartIndex = (page - 1) * Properties.Settings.Default.PagerNo;



            return View(drivers.ToList());

            //  return View(db.Vendors.ToList());
        }






        // GET: /Drivers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }

            ViewBag.VendorName = db.Vendors.Find(driver.VendorID).VendorName;


            return View(driver);
        }

        // GET: /Drivers/Create
        public ActionResult Create()
        {
            var user_id = User.Identity.GetUserId();

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == user_id).First().VendorId;

            ViewBag.vendor_id = vendor_id;

            return View();
        }

        // POST: /Drivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DriverId,DriverName,DriverDescription")] Driver driver)
        {

            var user_id = User.Identity.GetUserId();

            int vendor_id = db.Vendors.Where(v => v.ApplicationUserId == user_id).First().VendorId;

            driver.VendorID = vendor_id;
            driver.DriverActive = true;
            driver.DriverValidation = DriverValidationType.Pending;

            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDriversData);
                var storageContainerImages = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDriversData);
                

                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader2")
                    {
                        fileName = "driver_" + Server.UrlEncode(vendor_id.ToString() + driver.DriverName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            driver.DriverUrl = azureBlockBlob.Uri.ToString();
                        }


                    }



                }
            }



            if (ModelState.IsValid)
            {
                db.Drivers.Add(driver);
                db.SaveChanges();

                return RedirectToAction("VendorIndex", "Drivers", new { id = user_id });
            }

            return View(driver);

        }

        // GET: /Drivers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // GET: /Drivers/Edit/5
        public ActionResult EditVendor(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }


        // POST: /Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DriverId,DriverName,DriverUrl,DriverSelf,DriverDescription,DriverActive,DriverValidation,VendorID")] Driver driver)
        {




            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDriversData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader2")
                    {
                        fileName = "driver_" + Server.UrlEncode(driver.VendorID.ToString() + driver.DriverName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            driver.DriverUrl = azureBlockBlob.Uri.ToString();
                        }


                    }



                }
            }


            if (ModelState.IsValid)
            {
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(driver);


        }


        // POST: /Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVendor([Bind(Include = "DriverId,DriverName,DriverUrl,DriverSelf,DriverDescription,DriverActive,DriverValidation,VendorID")] Driver driver)
        {

            var user_id = User.Identity.GetUserId();


            var storage_connection_string = Properties.Settings.Default.StorageConnectionString;
            //CloudConfigurationManager.GetSetting("StorageConnectionString");
            //Request.Files.Count > 0

            var files = Request.Files;

            var current_file = Request.Files[0];


            if (current_file.ContentLength > 0)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storage_connection_string);
                var storageClient = storageAccount.CreateCloudBlobClient();
                var storageContainer = storageClient.GetContainerReference(
                Properties.Settings.Default.CloudStorageContainerReferenceDriversData);
                storageContainer.CreateIfNotExists();
                for (int fileNum = 0; fileNum < Request.Files.Count; fileNum++)
                {

                    var file_source = Request.Files.AllKeys[fileNum];

                    CloudBlockBlob azureBlockBlob;
                    string fileName;

                    if (file_source == "FileUploader2")
                    {
                        fileName = "driver_" + Server.UrlEncode(driver.VendorID.ToString() + driver.DriverName) + DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                        if (Request.Files[fileNum] != null && Request.Files[fileNum].ContentLength > 0)
                        {
                            azureBlockBlob = storageContainer.GetBlockBlobReference(fileName);
                            azureBlockBlob.UploadFromStream(Request.Files[fileNum].InputStream);
                            driver.DriverUrl = azureBlockBlob.Uri.ToString();
                        }


                    }



                }
            }


            if (ModelState.IsValid)
            {
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
           
                return RedirectToAction("VendorIndex", "Drivers", new { id = user_id });
            }
            return View(driver);


        }

        // GET: /Drivers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // POST: /Drivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Driver driver = db.Drivers.Find(id);
            db.Drivers.Remove(driver);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
