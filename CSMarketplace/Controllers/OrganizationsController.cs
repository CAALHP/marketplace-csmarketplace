﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using CSMarketplace.Models;
using System.Data.Entity.Validation;
using System.Text.RegularExpressions;

namespace CSMarketplace.Controllers
{
    public class OrganizationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public OrganizationsController()
            : this(new UserManager<User>(new UserStore<User>(new ApplicationDbContext())))
        {
        }

        public OrganizationsController(UserManager<User> userManager)
        {
           // UserManager = userManager;
           // var userValidator = UserManager.UserValidator as UserValidator<User>;
           // userValidator.AllowOnlyAlphanumericUserNames = false;

            UserManager = userManager;

            // Start of new code
            UserManager.UserValidator = new UserValidator<User>(UserManager)
            {
                AllowOnlyAlphanumericUserNames = false,
            };

        }


        public UserManager <User> UserManager { get; private set; }


        // GET: Organizations
        public ActionResult Index()
        {
            var organizations = db.Organizations.Include(o => o.AdministratorUser);
            return View(organizations.ToList());
        }

        // GET: Organizations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // GET: Organizations/Create
        public ActionResult Create()
        {
            //ViewBag.AdministratorUserId = new SelectList(db.Users, "Id", "UserName");

           // ManageOrganizationViewModel model = new ManageOrganizationViewModel();

          //  return View(model);
            return View();
        }

        // POST: Organizations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrganizationName,OrganizationAddress,OrganizationPhone,OrganizationFax,OrganizationWebLink,OrganizationActive,FirstName,LastName,UserName,Password,ConfirmPassword")] ManageOrganizationViewModel managedOrganization)
        {

            
                var Db = new ApplicationDbContext();

                if (string.IsNullOrEmpty(managedOrganization.OrganizationName))
                {
                    ModelState.AddModelError("OrganizationName", "Organization Name is required");
                }


                if (string.IsNullOrEmpty(managedOrganization.FirstName))
                {
                    ModelState.AddModelError("FirstName", "Manager First Name is required");
                }

                if (string.IsNullOrEmpty(managedOrganization.LastName))
                {
                    ModelState.AddModelError("LastName", "Manager Last Name is required");
                }

                if (!string.IsNullOrEmpty(managedOrganization.UserName))
                {
                    string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                             @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                    Regex re = new Regex(emailRegex);
                    if (!re.IsMatch(managedOrganization.UserName))
                    {
                        ModelState.AddModelError("UserName", "Email is not valid");
                    }
                }
                else
                {
                    ModelState.AddModelError("UserName", "Email is required");
                }



               int same_name = db.Users.Where(u => u.UserName == managedOrganization.UserName).ToList().Count();

               if (same_name > 0)
               {

                   ModelState.AddModelError("UserName", "Email already exists in the Database");

               }


                if (ModelState.IsValid)
                {
                    
                


                var user = new User() { UserName = managedOrganization.UserName };
                user.FirstName = managedOrganization.FirstName;
                user.LastName = managedOrganization.LastName;

                var result = await UserManager.CreateAsync(user, managedOrganization.Password);
                string role_careadmin = "Care Administrator";

                // var store = new UserStore<ApplicationUser>(context);
                // var manager = new UserManager<ApplicationUser>(store);


                if (result.Succeeded)
                {
                    var result_role = await UserManager.AddToRoleAsync(user.Id, role_careadmin);

                    var current_user = await UserManager.FindByIdAsync(user.Id);

                    Organization new_organization = new Organization();
                    new_organization.AdministratorUserId = current_user.Id;
                    new_organization.OrganizationActive = managedOrganization.OrganizationActive;
                    new_organization.OrganizationName = managedOrganization.OrganizationName;
                    new_organization.OrganizationAddress = managedOrganization.OrganizationAddress;
                    new_organization.OrganizationPhone = managedOrganization.OrganizationPhone;
                    new_organization.OrganizationFax = managedOrganization.OrganizationFax;
                    new_organization.OrganizationWebLink = managedOrganization.OrganizationWebLink;
     
                        //new_vendor_account.ApplicationUser = current_user;



                     Db.Organizations.Add(new_organization);
                        try
                        {
                            await Db.SaveChangesAsync();
                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {

                                    var pp = validationError.PropertyName;
                                    var rr = validationError.ErrorMessage;
                                     var zz=0;
                                }
                            }
                        }

                        
                   
                    return RedirectToAction("Index", "Organizations");
                }
                else
                {
                    //AddErrors(result);
                }


                }


          //  if (ModelState.IsValid)
          //  {
               // db.Organizations.Add(organization);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
          //  }

           // ViewBag.AdministratorUserId = new SelectList(db.Users, "Id", "UserName", organization.AdministratorUserId);
            return View(managedOrganization);
        }

        // GET: Organizations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return HttpNotFound();
            }

            ManageOrganizationViewModel model = new ManageOrganizationViewModel();
            model.OrganizationId = organization.OrganizationId;
            model.OrganizationName = organization.OrganizationName;
            model.OrganizationAddress = organization.OrganizationAddress;
            model.OrganizationActive = organization.OrganizationActive;
            model.OrganizationPhone = organization.OrganizationPhone;
            model.OrganizationFax = organization.OrganizationFax;
            model.OrganizationWebLink = organization.OrganizationWebLink;

            model.UserId = organization.AdministratorUserId;
            model.FirstName = organization.AdministratorUser.FirstName;
            model.LastName = organization.AdministratorUser.LastName;
            model.UserName = organization.AdministratorUser.UserName;

            //ViewBag.AdministratorUserId = new SelectList(db.Users, "Id", "UserName", organization.AdministratorUserId);
            return View(model);
        }

        // POST: Organizations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrganizationId,OrganizationName,OrganizationAddress,OrganizationPhone,OrganizationFax,OrganizationWebLink,OrganizationActive,FirstName,LastName,UserName,UserId")] ManageOrganizationViewModel managedOrganization)
        {

            if ((managedOrganization.OrganizationId != 0) && (managedOrganization.UserId != null))
            {
           
           // var Db = new ApplicationDbContext();

           // User current_user = await UserManager.FindByIdAsync(managedOrganization.UserId);

            User current_user = db.Users.Find(managedOrganization.UserId);

            current_user.FirstName = managedOrganization.FirstName;
            current_user.LastName = managedOrganization.LastName;
            current_user.UserName = managedOrganization.UserName;

            db.Entry(current_user).State = EntityState.Modified;
            await db.SaveChangesAsync();



            Organization organization = db.Organizations.Find(managedOrganization.OrganizationId);

            organization.OrganizationActive = managedOrganization.OrganizationActive;
            organization.OrganizationName = managedOrganization.OrganizationName;
            organization.OrganizationAddress = managedOrganization.OrganizationAddress;
            organization.OrganizationPhone = managedOrganization.OrganizationPhone;
            organization.OrganizationFax = managedOrganization.OrganizationFax;
            organization.OrganizationWebLink = managedOrganization.OrganizationWebLink;

            db.Entry(organization).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction("Index", "Organizations");

            }
            
            /*if (ModelState.IsValid)
            {
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }*/
                     
            return View(managedOrganization);
        }



        // GET: Organizations/Edit/5
        public ActionResult ManagerUpdate(string id)
        {

            int organization_id = db.Organizations.Where(o => o.AdministratorUserId == id).First().OrganizationId;


            if (organization_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = db.Organizations.Find(organization_id);
            if (organization == null)
            {
                return HttpNotFound();
            }


            //ViewBag.AdministratorUserId = new SelectList(db.Users, "Id", "UserName", organization.AdministratorUserId);
            return View(organization);
        }

        // POST: Organizations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManagerUpdate([Bind(Include = "OrganizationId,OrganizationName,OrganizationAddress,OrganizationPhone,OrganizationFax,OrganizationWebLink,OrganizationActive,AdministratorUserId, AdministratorUser")] Organization managedOrganization)
        {

            if ((managedOrganization.OrganizationId != 0) && (managedOrganization.AdministratorUserId != null))
            {



                db.Entry(managedOrganization).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return RedirectToAction("Index", "Home");

            }

            /*if (ModelState.IsValid)
            {
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }*/

            return View(managedOrganization);
        }

        // GET: Organizations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // POST: Organizations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Organization organization = db.Organizations.Find(id);
            string userId = organization.AdministratorUserId; 

            db.Organizations.Remove(organization);
            db.SaveChanges();

            var user = db.Users.First(u => u.Id == userId);
            db.Users.Remove(user);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
