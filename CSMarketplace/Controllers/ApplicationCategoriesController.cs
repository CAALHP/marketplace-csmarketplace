﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSMarketplace.Models;

namespace CSMarketplace.Controllers
{
    public class ApplicationCategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ApplicationCategories
        public ActionResult Index()
        {
            return View(db.ApplicationCategories.ToList());
        }

        // GET: ApplicationCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationCategory applicationCategory = db.ApplicationCategories.Find(id);
            if (applicationCategory == null)
            {
                return HttpNotFound();
            }
            return View(applicationCategory);
        }

        // GET: ApplicationCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApplicationCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationCategoryId,ApplicationCategoryName,ApplicationCategoryActive,ApplicationCategoryIcon")] ApplicationCategory applicationCategory)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationCategories.Add(applicationCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationCategory);
        }

        // GET: ApplicationCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationCategory applicationCategory = db.ApplicationCategories.Find(id);
            if (applicationCategory == null)
            {
                return HttpNotFound();
            }
            return View(applicationCategory);
        }

        // POST: ApplicationCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationCategoryId,ApplicationCategoryName,ApplicationCategoryActive,ApplicationCategoryIcon")] ApplicationCategory applicationCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationCategory);
        }

        // GET: ApplicationCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationCategory applicationCategory = db.ApplicationCategories.Find(id);
            if (applicationCategory == null)
            {
                return HttpNotFound();
            }
            return View(applicationCategory);
        }

        // POST: ApplicationCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApplicationCategory applicationCategory = db.ApplicationCategories.Find(id);

            db.Database.ExecuteSqlCommand("UPDATE Applications SET ApplicationCategoryID = null WHERE ApplicationCategoryID={0}", id);

            db.ApplicationCategories.Remove(applicationCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
