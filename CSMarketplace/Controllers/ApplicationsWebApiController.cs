﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CSMarketplace.Models;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace CSMarketplace.Controllers
{

    
    public class ApplicationsWebApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/applications
        [Authorize(Roles = "CAALHP")] 
        [Route("api/applications")]
        public IEnumerable<MarketplaceContract.Application> GetApplications()
        {


            IList<MarketplaceContract.Application> applicationsList = new List<MarketplaceContract.Application>();

            CloudStorageAccount account = CloudStorageAccount.Parse(Properties.Settings.Default.StorageConnectionString);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference("applications");


            var applications = db.Applications.Where(a=>a.ApplicationValidation == ApplicationValidationType.Validated).ToList();
            foreach (var appData in applications)
            {

                var signedBlobUrl = "";

                if (!String.IsNullOrEmpty(appData.ApplicationUrl))
                {


                

                var application_filename = Path.GetFileName(Uri.UnescapeDataString(appData.ApplicationUrl).Replace("/", "\\"));
                var blob = container.GetBlockBlobReference(application_filename);

                var builder = new UriBuilder(blob.Uri);
                builder.Query = blob.GetSharedAccessSignature(
                    new SharedAccessBlobPolicy
                    {
                        Permissions = SharedAccessBlobPermissions.Read,
                        SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                        SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(60))
                    }).TrimStart('?');

                signedBlobUrl = builder.Uri.ToString();

                }


                applicationsList.Add(new MarketplaceContract.Application()
                {
                    ApplicationId = appData.ApplicationId,
                    Name = appData.ApplicationName,
                    Url = signedBlobUrl,
                    Icon = appData.ApplicationLogo
                });
            }

             return applicationsList;
        }

        // GET: api/applications/5
        [Authorize(Roles = "CAALHP")] 
        [Route("api/applications/{id}")]
        [ResponseType(typeof(MarketplaceContract.ApplicationProfile))]
        public IHttpActionResult GetApplication(int id)
        {
            Application application_db = db.Applications.Find(id);


            if (application_db == null)
            {
                return NotFound();
            }

            MarketplaceContract.ApplicationProfile application = new MarketplaceContract.ApplicationProfile();

            CloudStorageAccount account = CloudStorageAccount.Parse(Properties.Settings.Default.StorageConnectionString);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference("applications");


            var signedBlobUrl = "";

            if (!String.IsNullOrEmpty(application_db.ApplicationUrl))
            {


                var application_filename = Path.GetFileName(Uri.UnescapeDataString(application_db.ApplicationUrl).Replace("/", "\\"));
                var blob = container.GetBlockBlobReference(application_filename);

                var builder = new UriBuilder(blob.Uri);
                builder.Query = blob.GetSharedAccessSignature(
                    new SharedAccessBlobPolicy
                    {
                        Permissions = SharedAccessBlobPermissions.Read,
                        SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                        SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(60))
                    }).TrimStart('?');

                signedBlobUrl = builder.Uri.ToString();
            }

            application.ApplicationId = application_db.ApplicationId;
            application.Name = application_db.ApplicationName;
            application.Description = application_db.ApplicationDescription;
            application.Icon = application_db.ApplicationLogo;
            application.Url = signedBlobUrl;

             var query = from s in  db.ApplicationScreens
                         where (s.ApplicationID == application.ApplicationId) 
                         select s;


            var screens = query.ToList();

            List<string> myScreensCollection = new List<string>();

            foreach (var myScreen in screens)
            {
                myScreensCollection.Add(myScreen.ApplicationScreenUrl);
            }


            application.ImagesUrls = myScreensCollection.ToArray();

            return Ok(application);
        }

      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationExists(int id)
        {
            return db.Applications.Count(e => e.ApplicationId == id) > 0;
        }
    }
}