﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSMarketplace.Models
{
    public class DeviceScreen
    {

        public int DeviceScreenId { get; set; }
        public string DeviceScreenUrl { get; set; }
        public int DeviceScreenSortIndex { get; set; }

        public int DeviceID { get; set; }
        public virtual Device Device { get; set; }


    }
}