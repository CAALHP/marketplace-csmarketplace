﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Globalization;
using System;

namespace CSMarketplace.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {   //ApplicationUser
       // UserManager.UserValidator = new UserValidator<TUser>(UserManager) { AllowOnlyAlphanumericUserNames = false }

                  // UserValidator = new UserValidator<ApplicationUser>(this) { AllowOnlyAlphanumericUserNames = false };
       


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public Nullable<int> OrganizationRelationID { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
        public virtual ICollection<Area> Areas { get; set; }
        public virtual ICollection<Platform> Platforms { get; set; }
        public virtual ICollection<Credential> Credentials { get; set; }
        public virtual ICollection<DriverVersion> DriverVersions { get; set; }
        public virtual ICollection<ApplicationVersion> ApplicationVersions { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Driver> Drivers { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Application> Applications { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Page> Pages { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.EditUserViewModel> EditUserViewModels { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.SelectUserRolesViewModel> SelectUserRolesViewModels { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.SelectRoleEditorViewModel> SelectRoleEditorViewModels { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Vendor> Vendors { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.ApplicationScreen> ApplicationScreens { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.ApplicationCategory> ApplicationCategories { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Device> Devices { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.DeviceCategory> DeviceCategories { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.DeviceScreen> DeviceScreens { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Organization> Organizations { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Credential> Credentials { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Area> Areas { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.Platform> Platforms { get; set; }

        public System.Data.Entity.DbSet<CSMarketplace.Models.DeviceEntity> DeviceEntities { get; set; }

    //    public System.Data.Entity.DbSet<CSMarketplace.Models.User> IdentityUsers { get; set; }

      //  public System.Data.Entity.DbSet<CSMarketplace.Models.User> IdentityUsers { get; set; }

       // public System.Data.Entity.DbSet<CSMarketplace.Models.User> IdentityUsers { get; set; }

        
    }

    public class IdentityManager
    {
        public bool RoleExists(string name)
        {
            var rm = new RoleManager<IdentityRole>(
                new RoleStore<IdentityRole>(new ApplicationDbContext()));
            return rm.RoleExists(name);
        }


        public bool CreateRole(string name)
        {
            var rm = new RoleManager<IdentityRole>(
                new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var idResult = rm.Create(new IdentityRole(name));
            return idResult.Succeeded;
        }


        public bool CreateUser(User user, string password)
        {
            var um = new UserManager<User>(
                new UserStore<User>(new ApplicationDbContext()));
            var idResult = um.Create(user, password);
            return idResult.Succeeded;
        }


        public bool AddUserToRole(string userId, string roleName)
        {
            var um = new UserManager<User>(
                new UserStore<User>(new ApplicationDbContext()));
            var idResult = um.AddToRole(userId, roleName);
            return idResult.Succeeded;
        }


        public void ClearUserRoles(string userId)
        {
            var um = new UserManager<User>(
                new UserStore<User>(new ApplicationDbContext()));
            var user = um.FindById(userId);
            var currentRoles = new List<IdentityUserRole>();
            currentRoles.AddRange(user.Roles);
            foreach (var role in currentRoles)
            {
                
               // um.RemoveFromRole(userId, role.Role.Name);
                um.RemoveFromRole(userId,role.RoleId);
              
            }
        }
    }


}