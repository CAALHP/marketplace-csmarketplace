﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSMarketplace.Models
{
    public class DeviceCategory
    {

        public int DeviceCategoryId { get; set; }
        public string DeviceCategoryName { get; set; }
        public Boolean DeviceCategoryActive { get; set; }
        public string DeviceCategoryIcon { get; set; }


        public virtual ICollection<Device> Devices { get; set; }



    }
}