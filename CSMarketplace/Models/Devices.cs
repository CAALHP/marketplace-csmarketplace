﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;


namespace CSMarketplace.Models
{
    public class Device
    {

        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceUrl { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string DeviceDescription { get; set; }
        public Boolean DeviceActive { get; set; }
        public DeviceValidationType DeviceValidation { get; set; }
        public string DeviceIcon { get; set; }
        public string DeviceSelf
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture,
                    "restapi/devices/{0}", this.DeviceId);
            }
            set { }
        }
        public Nullable<int> VendorID { get; set; }
        public virtual Vendor Vendor { get; set; }

        public Nullable<int> DeviceCateogryID { get; set; }
        public virtual DeviceCategory DeviceCategory { get; set; }

        public virtual ICollection<DeviceEntity> DeviceEntities { get; set; }
        public virtual ICollection<DeviceScreen> DeviceScreens { get; set; }
        public virtual ICollection<DriverVersion> DriverVersions { get; set; }
        public virtual ICollection<ApplicationVersion> ApplicationVersions { get; set; }
        public virtual ICollection<Driver> Drivers { get; set; }

    }


    public enum DeviceValidationType
    {
        [Display(Name = "Validation Pending")]
        Pending,
        [Display(Name = "Validated")]
        Validated,
        [Display(Name = "Not Validated")]
        Notvalidated
    }




   }
