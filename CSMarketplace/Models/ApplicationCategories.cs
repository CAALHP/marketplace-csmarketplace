﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSMarketplace.Models
{
    public class ApplicationCategory
    {
        public int ApplicationCategoryId { get; set; }
        public string ApplicationCategoryName { get; set; }
        public Boolean ApplicationCategoryActive { get; set; }
        public string ApplicationCategoryIcon { get; set; }


        public virtual ICollection<Application> Applications { get; set; }



    }
}