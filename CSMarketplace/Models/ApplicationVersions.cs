﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class ApplicationVersion
    {

        public int ApplicationVersionId { get; set; }
        public string ApplicationVersionName { get; set; }
        public string ApplicationVersionUrl { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string ApplicationVersionDescription { get; set; }
        public Boolean ApplicationVersionActive { get; set; }
        public string ApplicationVersionValidationRemark { get; set; }
        public ApplicationVersionValidationType ApplicationVersionValidation { get; set; }
        public string ApplicationVersionSelf
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture,
                    "restapi/appversion/{0}", this.ApplicationVersionId);
            }
            set { }
        }
        public DateTime ApplicationUploadDate { get; set; }

        public int ApplicationID { get; set; }
        public virtual Application Application { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<Platform> Platforms { get; set; }

    }

    public enum ApplicationVersionValidationType
    {
        [Display(Name = "Validation Pending")]
        Pending,
        [Display(Name = "Validated")]
        Validated,
        [Display(Name = "Not Validated")]
        Notvalidated
    }
}