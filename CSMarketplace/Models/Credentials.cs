﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class Credential
    {

        public int CredentialId { get; set; }
        public string CredentialData { get; set; }
        public CredentialMetricType CredentialMetric { get; set; }
        public string CarestoreUserId { get; set; }
        public virtual User CarestoreUser { get; set; }

    }

    public enum CredentialMetricType
    {
        [Display(Name = "PIN")]
        PIN,
        [Display(Name = "FINGERPRINT")]
        FINGERPRINT,
        [Display(Name = "FACE")]
        FACE,
        [Display(Name = "HAND")]
        HAND,
        [Display(Name = "IRIS")]
        IRIS,
        [Display(Name = "NFC")]
        NFC
    }
}