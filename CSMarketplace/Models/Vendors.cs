﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class Vendor
    {

        public int VendorId { get; set; }
        public string VendorName { get; set; }
        [DataType(DataType.MultilineText)]
        public string VendorAddress { get; set; }
        public string VendorPhone { get; set; }
        public string VendorFax { get; set; }
        public string VendorWebLink { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string VendorDescription { get; set; }
        public Boolean VendorActive { get; set; }
        public VendorValidationType VendorValidation { get; set; }
        public string VendorLogo { get; set; }
        public string VendorSelf
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture,
                    "restapi/vendors/{0}", this.VendorId);
            }
            set { }
        }
        public string ApplicationUserId { get; set; }
        public virtual User ApplicationUser { get; set; }
        public virtual ICollection<Driver> Drivers { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
    }

    public enum VendorValidationType
    {
        [Display(Name = "Validation Pending")]
        Pending,
        [Display(Name = "Validated")]
        Validated,
        [Display(Name = "Not Validated")]
        Notvalidated
    }
}