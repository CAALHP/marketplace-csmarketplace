﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class Area
    {

        public int AreaId { get; set; }
        public string AreaName { get; set; }
        [DataType(DataType.MultilineText)]
        public string AreaAddress { get; set; }
        public int AreaSortIndex { get; set; }


        public Nullable<int> AreaParentId { get; set; }
        public virtual Area AreaParent { get; set; }


        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public virtual ICollection<User> CareGivers { get; set; }
        public virtual ICollection<Platform> CarePlatforms { get; set; }


    }


    public class AreaNode:Area
    {
        public List<AreaNode> Children = new List<AreaNode>();
    }

    public class AreaTree
    {
        private AreaNode rootNode;
        public AreaNode RootNode
        {
            get { return rootNode; }
            set
            {
                if (RootNode != null)
                    Nodes.Remove(RootNode.AreaId);

                Nodes.Add(value.AreaId, value);
                rootNode = value;
            }
        }

        public Dictionary <int,AreaNode> Nodes { get; set; }

        public AreaTree()
        {
        }

        public void BuildAreaTree()
        {
            AreaNode parent;
            foreach (var node in Nodes.Values)
            {

                if (node.AreaParentId == null)
                {
                    parent = RootNode;
                    node.AreaParent = parent;
                    parent.Children.Add(node);

                }
                else
                {

                

                    if (Nodes.TryGetValue((int)node.AreaParentId, out parent) &&
                        node.AreaId != node.AreaParentId)
                    {
                        node.AreaParent = parent;
                        parent.Children.Add(node);
                    }

                }

            }
        }
    }

}