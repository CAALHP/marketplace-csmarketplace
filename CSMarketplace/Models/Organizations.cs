﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CSMarketplace.Models
{
    public class Organization
    {

        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        [DataType(DataType.MultilineText)]
        public string OrganizationAddress { get; set; }
        public string OrganizationPhone { get; set; }
        public string OrganizationFax { get; set; }
        public string OrganizationWebLink { get; set; }
        public Boolean OrganizationActive { get; set; }

        public string AdministratorUserId { get; set; }
        public virtual User AdministratorUser { get; set; }

        public virtual ICollection<DeviceEntity> DeviceEntities { get; set; }
        public virtual ICollection<Platform> CarePlatforms { get; set; }
    }

    public class ManageOrganizationViewModel
    {
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        [DataType(DataType.MultilineText)]
        public string OrganizationAddress { get; set; }
        public string OrganizationPhone { get; set; }
        public string OrganizationFax { get; set; }
        public string OrganizationWebLink { get; set; }
        public Boolean OrganizationActive { get; set; }


        public string UserId { get; set; }
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage =
            "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        // New Fields added to extend Application User class:
        [Display(Name = "First Name")]
        public string FirstName { get; set; }


        [Display(Name = "Last Name")]
        public string LastName { get; set; }


        // Return a pre-poulated instance of AppliationUser:
       /* public User GetUser()
        {
            var user = new User()
            {
                UserName = this.UserName,
                FirstName = this.FirstName,
                LastName = this.LastName,
            };
            return user;
        }*/

    }
}