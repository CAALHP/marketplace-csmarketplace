﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSMarketplace.Models
{
    public class DeviceEntity
    {
        public int DeviceEntityId { get; set; }
        public string DeviceEntityName { get; set; }
        public string DeviceEntityMAC { get; set; }
        public string DeviceEntityPairingCode { get; set; }
        public string DeviceEntityNFC { get; set; }

        public string OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public int DeviceID { get; set; }
        public virtual Device Device { get; set; }

    }
}