﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class DriverVersion
    {
        public int DriverVersionId { get; set; }
        public string DriverVersionName { get; set; }
        public string DriverVersionUrl { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string DriverVersionDescription { get; set; }
        public Boolean DriverVersionActive { get; set; }
        public string DriverVersionValidationRemark { get; set; }
        public DriverVersionValidationType DriverVersionValidation { get; set; }
        public string DriverVersionSelf
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture,
                    "restapi/driverversion/{0}", this.DriverVersionId);
            }
            set { }
        }
        public DateTime DriverUploadDate { get; set; }


        public int DriverID { get; set; }
        public virtual Driver Driver { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<Platform> Platforms { get; set; }


    }

    public enum DriverVersionValidationType
    {
        [Display(Name = "Validation Pending")]
        Pending,
        [Display(Name = "Validated")]
        Validated,
        [Display(Name = "Not Validated")]
        Notvalidated
    }
}