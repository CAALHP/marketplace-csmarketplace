﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class Driver
    {

        public int      DriverId    { get; set; }
        public string   DriverName  { get; set; }
        public string   DriverUrl   { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string DriverDescription { get; set; }
        public Boolean DriverActive { get; set; }
        public DriverValidationType DriverValidation { get; set; }
        public string   DriverSelf
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture,
                    "restapi/drivers/{0}", this.DriverId);
            }
            set { }
        }
        public Nullable<int> VendorID { get; set; }
        public virtual Vendor Vendor { get; set; }

        public virtual ICollection<DriverVersion> DriverVersions { get; set; }
        public virtual ICollection<Device> Devices { get; set; }

    }

    public enum DriverValidationType
    {
        [Display(Name = "Validation Pending")]
        Pending,
        [Display(Name = "Validated")]
        Validated,
        [Display(Name = "Not Validated")]
        Notvalidated
    }
}