﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSMarketplace.Models
{
    public class Page
    {


        public int PageId { get; set; }
        public string PageTitle { get; set; }
        public string PageSubTitle { get; set; }
        public string PageTag { get; set; }
        [UIHint("tinymce_jquery_full"),AllowHtml]
        public string PageContent { get; set; }
        



    }
}