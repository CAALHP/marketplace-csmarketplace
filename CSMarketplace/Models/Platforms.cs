﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSMarketplace.Models
{
    public class Platform
    {
      //  [Key, DatabaseGenerated       (         DatabaseGenerationOption.  )]
        
        public int PlatformId { get; set; }

      //  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PlatformGUID { get; set; }
        public string PlatformName { get; set; }
        public string PlatformMAC { get; set; }
        public string PlatformOS { get; set; }

        public int CareAreaId { get; set; }
        public virtual Area CareArea { get; set; }

        public int CareOrganizationId { get; set; }
        public virtual Organization CareOrganization { get; set; }

        public virtual ICollection<User> Citizens { get; set; }

        public virtual ICollection<DriverVersion> DriverVersions { get; set; }
        public virtual ICollection<ApplicationVersion> ApplicationVersions { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
        
    }
}