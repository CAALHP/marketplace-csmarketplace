﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace CSMarketplace.Models
{
    public class Application
    {

        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationUrl { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string ApplicationDescription { get; set; }
        public Boolean ApplicationActive { get; set; }
        public ApplicationValidationType ApplicationValidation { get; set; }
        /// <summary>
        /// Path to Application Icon
        /// </summary>
        /// <value>URL</value>
        public string ApplicationLogo { get; set; }
        public string ApplicationSelf
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture,
                    "restapi/applications/{0}", this.ApplicationId);
            }
            set { }
        }
        public Nullable <int> VendorID { get; set; }        
        public virtual Vendor Vendor { get; set; }

        public Nullable <int> ApplicationCategoryID { get; set; }
        public virtual ApplicationCategory ApplicationCategory { get; set; }

        public virtual ICollection<ApplicationScreen> ApplicationScreens { get; set; }
        public virtual ICollection<ApplicationVersion> ApplicationVersions { get; set; }
    }


    public enum ApplicationValidationType
    {
        [Display(Name = "Validation Pending")]
        Pending,
        [Display(Name = "Validated")]
        Validated,
        [Display(Name = "Not Validated")]
        Notvalidated
    }

}