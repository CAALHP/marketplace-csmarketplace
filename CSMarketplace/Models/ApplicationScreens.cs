﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
namespace CSMarketplace.Models
{
    public class ApplicationScreen
    {

        public int ApplicationScreenId { get; set; }
        public string ApplicationScreenUrl { get; set; }
        public int ApplicationScreenSortIndex { get; set; }

        public int ApplicationID { get; set; }
        public virtual Application Application { get; set; }

    }
}