﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace CSMarketplace.Other
{
    public class CustomFileResult : FileContentResult
    {

        public string Content { get; private set; }

        public string DownloadFileName { get; private set; }

        public CustomFileResult(string content, string contentType, string downloadFileName)
            : base(Encoding.ASCII.GetBytes(content), contentType)
        {
            Content = content;
            DownloadFileName = downloadFileName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.AppendHeader("Content-Disposition", "attachment; filename=" + DownloadFileName);

            base.ExecuteResult(context);
        }

    }
}