namespace CSMarketplace.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.Validation;
    using CSMarketplace.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<CSMarketplace.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(CSMarketplace.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            string role_admin = "Admin";
            string role_careadmin = "Care Administrator";
            string role_caregiver = "Care Giver";
            string role_citizen = "Citizen";
            string role_vendor = "Vendor";
            string role_caalhp = "CAALHP";

            //Create Roles if they does not exist
            if (!RoleManager.RoleExists(role_admin))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role_admin));
            }
            if (!RoleManager.RoleExists(role_careadmin))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role_careadmin));
            }
            if (!RoleManager.RoleExists(role_caregiver))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role_caregiver));
            }
            if (!RoleManager.RoleExists(role_citizen))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role_citizen));
            }
            if (!RoleManager.RoleExists(role_vendor))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role_vendor));
            }
            if (!RoleManager.RoleExists(role_caalhp))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role_caalhp));
            }


            if (!context.Users.Any(u => u.UserName == "admin@carestore.eu"))
            {
                var store = new UserStore<User>(context);
                var manager = new UserManager<User>(store);

                manager.UserValidator = new UserValidator<User>(manager)
                {
                    AllowOnlyAlphanumericUserNames = false
                };


                var user = new User
                {
                    UserName = "admin@carestore.eu",
                    FirstName = "Carestore",
                    LastName = "Administrator"

                };

                manager.Create(user, "123456");
                manager.AddToRole(user.Id, role_admin);
            }

            if (!context.Users.Any(u => u.UserName == "caalhp@carestore.eu"))
            {
                var store2 = new UserStore<User>(context);
                var manager2 = new UserManager<User>(store2);

                manager2.UserValidator = new UserValidator<User>(manager2)
                {
                    AllowOnlyAlphanumericUserNames = false
                };


                var user2 = new User
                {
                    UserName = "caalhp@carestore.eu",
                    FirstName = "Carestore",
                    LastName = "CAALHP"

                };

                manager2.Create(user2, "123456");
                manager2.AddToRole(user2.Id, role_caalhp);
            }
        }
    }
}
