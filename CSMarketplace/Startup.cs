﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CSMarketplace.Startup))]
namespace CSMarketplace
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {


            ConfigureAuth(app);
        }
    }
}
