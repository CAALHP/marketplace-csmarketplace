// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();	

$( ".upload" ).each(function() {
	$(this).change(function(){

		var $obj_file = $(this).parents().eq(2).find('.upfile');
		$obj_file.val($(this).val());

	});    
});

$('#my-select').multiSelect({
  selectableHeader: "<p>Available devices</p><input type='text' class='search-input' autocomplete='off' placeholder='try \"12\"'>",
  selectionHeader: "<p>Picked devices</p><input type='text' class='search-input' autocomplete='off' placeholder='try \"4\"'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});


$(function(){

  var mySwiper = $('.swiper-container').swiper({
    mode:'horizontal',
    pagination : '.pagination1',
    calculateHeight: true,
    cssWidthAndHeight: true
  });

  //pagination
  $('.pagination1 .swiper-pagination-switch').click(function(){
    mySwiper.swipeTo($(this).index())
  })  

  var $switch_count  = $('.pagination1 .swiper-pagination-switch').length,
      $switches_width = $('.pagination1 .swiper-pagination-switch').outerWidth(true)*$switch_count;

  $('.pagination1').css({'width':$switches_width, 'margin-left': -$switches_width/2 });

  //remove slide

  $('.remove-slide').click(function(){
    var $active_slide = mySwiper.activeSlide().index();
    mySwiper.removeSlide($active_slide);

    mySwiper.swipeTo( $active_slide - 1, 900, false );
    mySwiper.reInit();

  });

})

$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_minimal-grey',
    radioClass: 'iradio_minimal-grey',
    increaseArea: '20%' // optional
  });
});




